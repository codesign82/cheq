<?php wp_footer(); ?>
<?php
$first_column    = get_field( 'first_column', 'options' );
$second_column   = get_field( 'second_column', 'options' );
$third_column    = get_field( 'third_column', 'options' );
$newsletter_form = get_field( 'newsletter_form', 'options' );
$instagram       = get_field( 'instagram', 'options' );
$facebook        = get_field( 'facebook', 'options' );
$twitter         = get_field( 'twitter', 'options' );
$youtube         = get_field( 'youtube', 'options' );
$linked_in       = get_field( 'linked_in', 'options' );
$copyright_text  = get_field( 'copyright_text', 'options' );
?>
<!--region footer-->

<footer>
  <div class="container">
    <div class="footer-wrapper">
      <!--    title and links and logos-->
      <div class="footer-content">

        <!--  first column -->
        <div class="title-and-content">
          <?php if ( $first_column['title'] ) { ?>
            <h3 class="headline-3"><?= $first_column['title']; ?></h3>
          <?php } ?>
          <ul class="footer-links-content reset-ul">
            <?php
            if ( have_rows( 'first_column', 'options' ) ) :
              while ( have_rows( 'first_column', 'options' ) ) :
                the_row();
                ?>
                <?php if ( have_rows( 'links' ) ) : ?>
                <?php while ( have_rows( 'links' ) ) : the_row();
                  $link = get_sub_field( 'link' );
                  ?>
                  <?php if ( $link ) { ?>
                    <li class="link-text link">
                      <a href="<?= $link['url'] ?>"
                         target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
                    </li>
                  <?php } ?>
                <?php endwhile; ?>
              <?php endif; ?>
              <?php endwhile; endif; ?>
          </ul>
        </div>

        <!--  second column -->
        <div class="title-and-content">
          <?php if ( $second_column['title'] ) { ?>
            <h3 class="headline-3"><?= $second_column['title']; ?></h3>
          <?php } ?>
          <ul class="footer-links-content reset-ul">
            <?php
            if ( have_rows( 'second_column', 'options' ) ) :
              while ( have_rows( 'second_column', 'options' ) ) :
                the_row();
                ?>
                <?php if ( have_rows( 'links' ) ) : ?>
                <?php while ( have_rows( 'links' ) ) : the_row();
                  $link = get_sub_field( 'link' );
                  ?>
                  <?php if ( $link ) { ?>
                    <li class="link-text link">
                      <a href="<?= $link['url'] ?>"
                         target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
                    </li>
                  <?php } ?>
                <?php endwhile; ?>
              <?php endif; ?>
              <?php endwhile; endif; ?>
          </ul>
        </div>

        <!--  third column  -->
        <div class="title-and-content">
          <?php if ( $third_column['title'] ) { ?>
            <h3 class="headline-3"><?= $third_column['title']; ?></h3>
          <?php } ?>
          <ul class="social-icons-wrapper reset-ul">
            <?php if ( $third_column['facebook'] ) { ?>
              <li class="social-icon">
                <a aria-label="Facebook" rel="noopener"
                   href="<?= $third_column['facebook'] ?>">
                  <svg viewBox="0 0 11 20" fill="none"
                       xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M2.80805 20V10.6154H0V7.23652H2.80805V4.3505C2.80805 2.08264 4.46677 0 8.28882 0C9.83631 0 10.9806 0.1311 10.9806 0.1311L10.8904 3.28642C10.8904 3.28642 9.72344 3.27638 8.44996 3.27638C7.07167 3.27638 6.85085 3.83768 6.85085 4.7693V7.23652H11L10.8195 10.6154H6.85085V20H2.80805Z"
                      fill="#111222"/>
                  </svg>
                </a>
              </li>
            <?php } ?>
            <?php if ( $third_column['twitter'] ) { ?>
              <li class="social-icon">
                <a aria-label="twitter" rel="noopener"
                   href="<?= $third_column['twitter'] ?>">
                  <svg viewBox="0 0 24 20" fill="none"
                       xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M24 2.37575C23.1174 2.76628 22.157 3.04735 21.1676 3.15682C22.1948 2.53089 22.9639 1.54202 23.3308 0.375752C22.367 0.963885 21.3111 1.37629 20.2101 1.59469C19.7499 1.09003 19.1933 0.687999 18.575 0.413643C17.9567 0.139287 17.2899 -0.00151415 16.6163 1.22793e-05C13.8906 1.22793e-05 11.6986 2.26628 11.6986 5.04735C11.6986 5.43788 11.7447 5.82841 11.8197 6.20415C7.73849 5.98522 4.09855 3.98522 1.67864 0.923089C1.23771 1.69562 1.00665 2.57524 1.00949 3.47042C1.00949 5.2219 1.87766 6.76628 3.20154 7.67456C2.42136 7.64305 1.65944 7.42308 0.977767 7.03255V7.09468C0.977767 9.54734 2.66795 11.5799 4.92056 12.0473C4.49761 12.16 4.06251 12.2177 3.62553 12.2189C3.30537 12.2189 3.00252 12.1864 2.69679 12.142C3.31979 14.142 5.134 15.5947 7.29432 15.642C5.60413 17 3.48708 17.7988 1.18832 17.7988C0.775868 17.7988 0.395145 17.784 0 17.7367C2.18051 19.1716 4.7677 20 7.5539 20C16.599 20 21.5484 12.3136 21.5484 5.64202C21.5484 5.42309 21.5484 5.20415 21.534 4.98522C22.4915 4.26628 23.3308 3.37575 24 2.37575Z"
                      fill="#111222"/>
                  </svg>

                </a>
              </li>
            <?php } ?>
            <?php if ( $third_column['instagram'] ) { ?>
              <li class="social-icon">
                <a aria-label="Instagram" rel="noopener"
                   href="<?= $third_column['instagram'] ?>">
                  <svg viewBox="0 0 20 20" fill="none"
                       xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M9.99731 6.66525C8.16111 6.66525 6.66262 8.16376 6.66262 10C6.66262 11.8362 8.16111 13.3348 9.99731 13.3348C11.8335 13.3348 13.332 11.8362 13.332 10C13.332 8.16376 11.8335 6.66525 9.99731 6.66525ZM19.9989 10C19.9989 8.61907 20.0114 7.25064 19.9338 5.87221C19.8563 4.27113 19.4911 2.85017 18.3203 1.67938C17.147 0.506085 15.7286 0.14334 14.1275 0.065788C12.7466 -0.0117644 11.3782 0.000744113 9.99981 0.000744113C8.61891 0.000744113 7.25051 -0.0117644 5.8721 0.065788C4.27105 0.14334 2.85012 0.508587 1.67935 1.67938C0.506076 2.85267 0.143338 4.27113 0.0657868 5.87221C-0.0117642 7.25314 0.000744099 8.62157 0.000744099 10C0.000744099 11.3784 -0.0117642 12.7494 0.0657868 14.1278C0.143338 15.7289 0.508578 17.1498 1.67935 18.3206C2.85262 19.4939 4.27105 19.8567 5.8721 19.9342C7.25301 20.0118 8.62141 19.9993 9.99981 19.9993C11.3807 19.9993 12.7491 20.0118 14.1275 19.9342C15.7286 19.8567 17.1495 19.4914 18.3203 18.3206C19.4936 17.1473 19.8563 15.7289 19.9338 14.1278C20.0139 12.7494 19.9989 11.3809 19.9989 10ZM9.99731 15.131C7.15795 15.131 4.86644 12.8394 4.86644 10C4.86644 7.16058 7.15795 4.86903 9.99731 4.86903C12.8367 4.86903 15.1282 7.16058 15.1282 10C15.1282 12.8394 12.8367 15.131 9.99731 15.131ZM15.3383 5.8572C14.6754 5.8572 14.14 5.32184 14.14 4.65889C14.14 3.99594 14.6754 3.46058 15.3383 3.46058C16.0013 3.46058 16.5366 3.99594 16.5366 4.65889C16.5368 4.81631 16.5059 4.97222 16.4458 5.1177C16.3856 5.26317 16.2974 5.39535 16.1861 5.50666C16.0748 5.61798 15.9426 5.70624 15.7971 5.76639C15.6516 5.82654 15.4957 5.8574 15.3383 5.8572Z"
                      fill="#111222"/>
                  </svg>

                </a>
              </li>
            <?php } ?>
            <?php if ( $third_column['linked_in'] ) { ?>
              <li class="social-icon">
                <a aria-label="linkedin" rel="noopener"
                   href="<?= $third_column['linked_in'] ?>">
                  <svg viewBox="0 0 20 20" fill="none"
                       xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M17.0415 17.0413H14.078V12.4004C14.078 11.2937 14.0583 9.86912 12.5367 9.86912C10.9933 9.86912 10.7571 11.0749 10.7571 12.3199V17.041H7.79361V7.49741H10.6385V8.80165H10.6784C10.9631 8.31485 11.3745 7.91439 11.8688 7.64291C12.3631 7.37143 12.9218 7.23911 13.4854 7.26003C16.489 7.26003 17.0427 9.23567 17.0427 11.8059L17.0415 17.0413ZM4.44983 6.19287C4.1097 6.19293 3.77718 6.09213 3.49434 5.90321C3.21149 5.71429 2.99104 5.44574 2.86082 5.13153C2.7306 4.81731 2.69647 4.47153 2.76277 4.13792C2.82907 3.80431 2.99281 3.49785 3.23327 3.2573C3.47374 3.01675 3.78014 2.8529 4.11373 2.78649C4.44731 2.72007 4.79309 2.75406 5.10736 2.88417C5.42162 3.01428 5.69025 3.23465 5.87926 3.51743C6.06828 3.8002 6.16921 4.13268 6.16927 4.47282C6.16931 4.69866 6.12486 4.92229 6.03847 5.13095C5.95209 5.33962 5.82546 5.52922 5.66579 5.68895C5.50613 5.84867 5.31656 5.97538 5.10792 6.06184C4.89929 6.1483 4.67567 6.19282 4.44983 6.19287ZM5.93157 17.0413H2.96501V7.49741H5.93157V17.0413ZM18.5189 0.00136263H1.47587C1.08904 -0.00300274 0.716288 0.146386 0.439548 0.416702C0.162807 0.687018 0.00472104 1.05614 0 1.44297V18.5567C0.00455946 18.9438 0.162553 19.3131 0.439284 19.5837C0.716015 19.8543 1.08885 20.004 1.47587 19.9999H18.5189C18.9067 20.0047 19.2806 19.8555 19.5584 19.5849C19.8362 19.3143 19.9953 18.9445 20.0007 18.5567V1.44174C19.9951 1.05414 19.836 0.684596 19.5581 0.414287C19.2803 0.143978 18.9065 -0.00498453 18.5189 0.000127351"
                      fill="#111222"/>
                  </svg>
                </a>
              </li>
            <?php } ?>
            <?php if ( $third_column['youtube'] ) { ?>
              <li class="social-icon">
                <a aria-label="youtube" rel="noopener"
                   href="<?= $third_column['youtube'] ?>">
                  <svg viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M15.1572 1.8735C15.5 3.21 15.5 6 15.5 6C15.5 6 15.5 8.79 15.1572 10.1265C14.9667 10.8653 14.4095 11.4465 13.7037 11.643C12.422 12 8 12 8 12C8 12 3.58025 12 2.29625 11.643C1.5875 11.4435 1.031 10.863 0.84275 10.1265C0.5 8.79 0.5 6 0.5 6C0.5 6 0.5 3.21 0.84275 1.8735C1.03325 1.13475 1.5905 0.5535 2.29625 0.357C3.58025 -1.3411e-07 8 0 8 0C8 0 12.422 -1.3411e-07 13.7037 0.357C14.4125 0.5565 14.969 1.137 15.1572 1.8735ZM6.5 8.625L11 6L6.5 3.375V8.625Z"
                          fill="#111222"/>
                  </svg>
                </a>
              </li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <!--    five column  -->
      <div class="title-and-input">
        <?php if ( $newsletter_form['title'] ) { ?>
          <h3 class="headline-3"><?= $newsletter_form['title']; ?></h3>
        <?php } ?>
        <?php if ( $newsletter_form['form_code_snippet'] ) { ?>
          <template lazyload-script>
            <?php
            echo $newsletter_form['form_code_snippet'];
            ?>
          </template>
        <?php } ?>
      </div>
      <!--        copy right-->
      <?php if ( $copyright_text ) { ?>
        <span class="link-text"><?= $copyright_text ?></span>
      <?php } ?>
    </div>
  </div>
</footer>
<!--endregion footer-->
</div><!--end of page-transition -->
</main>
</body>
</html>
