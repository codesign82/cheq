<?php
get_header();
$blog_id = get_option('page_for_posts');

$title = get_field('title', $blog_id);
$description = get_field('description', $blog_id);
$featured_posts_title = get_field('featured_posts_title', $blog_id);
$categories_section_title = get_field('categories_section_title', $blog_id);


$recommended_resources_title = get_field('recommended_resources_title', $blog_id);

$explore_all_resources = get_field('explore_all_resources', $blog_id);
$posts_per_page = get_field('posts_per_page', $blog_id);


$fourth_section_title = get_field('fourth_section_title', $blog_id);
$fourth_section_description = get_field('fourth_section_description', $blog_id);
$form_shortcode = get_field('form_shortcode', $blog_id);

?>
  <div class="blog-page-wrapper">

    <section class="financial_tips_block" data-section-class="financial_tips_block">
      <div class="container large-padding">
        <div class="tools-wrapper">
          <div class="text iv-st-from-bottom">
            <h2 class="headline-1"><?= $title ?></h2>
            <div class="paragraph">
              <?= $description ?>
            </div>
          </div>
          <svg class="tree-branch iv-st-from-bottom" width="454" height="307" viewBox="0 0 454 307">
            <path fill="#82b378"
                  d="M263.672 226.69s-28.257-32.551-1.745-53.23c0 0 19.366-11.58 35.16-10.637 0 0-22.375 37.055-15.134 40.74 7.24 3.683 21.922-43.067 21.922-43.067s44.07-4.22 58.25 2.51c0 0-37.675 30.448-29.74 32.26 7.935 1.81 48.633-28.074 48.633-28.074s41.021 6.893 38.829 31.6c-2.185 24.709-16.323 32.341-22.533 31.683-6.211-.658-42.553-14.355-43.343-9.328-.79 5.027 21.084 21.451 36.397 22.218 0 0-28.916 31.492-48.138 22.408-19.222-9.09-20.857-23.5-32.839-28.9-11.98-5.407-22.21-5.394-17.264.122 4.947 5.508 26.628 10.868 33.485 22.835 6.849 11.968 12.997 20.041-6.884 17.083-19.889-2.958-52.205-17.32-53.627-33.779l-1.43-16.444z"/>
            <path fill="#82b378" d="M399.686 199.461s-111.41-4.24-136.012 27.225c0 0-5.441 16.092-22.183 30.746l-2.611 7.021s19.305-20.101 26.216-21.315c-.007 0-10.814-38.982 134.59-43.677z"/>
            <path fill="#82b378" d="M241.487 257.429s-23.914 17.462-25.577 48.67l4.342.38s7.64-32.266 21.44-44.885c13.81-12.612-.205-4.165-.205-4.165z"/>
            <path fill="#a8d29f"
                  d="M164.878 185.746s44.95-24.857 23.969-58.589c0 0-16.783-20.712-34.762-26.037 0 0 9.591 50.304 0 51.492-9.59 1.18-6.595-56.818-6.595-56.818s-47.348-22.49-65.924-20.712c0 0 29.369 49.124 19.778 47.937-9.59-1.18-42.552-50.895-42.552-50.895s-48.544-8.88-56.334 19.532c-7.79 28.405 4.795 42.611 11.988 44.389 7.193 1.777 53.338 1.18 52.143 7.103-1.195 5.916-32.364 15.386-49.746 10.06 0 0 19.181 46.758 44.346 44.39 25.172-2.368 32.962-17.754 48.544-18.942 15.581-1.187 26.971 2.958 19.181 7.103-7.791 4.146-34.158 1.357-46.744 11.92-12.586 10.563-22.774 17.076.598 21.811 23.372 4.736 65.327 1.778 73.715-15.976l8.395-17.768z"/>
            <path fill="#82b378" d="M24.64 100.52s125.858 40.244 140.236 85.223c0 0-.597 20.122 11.989 43.202v8.88s-13.184-30.183-20.377-34.328c0 0 28.167-39.056-131.848-102.977z"/>
            <path fill="#82b378" d="M176.874 228.943s19.408 29.104 8.347 64.532l-4.995-1.323s4.85-39.023-5.31-58.65c-10.154-19.62 1.958-4.559 1.958-4.559z"/>
            <path fill="#a8d29f"
                  d="M236.392 151.469s-59.247-32.767-31.602-77.231c0 0 22.12-27.306 45.822-34.328 0 0-12.64 66.315 0 67.869 12.641 1.56 8.69-74.89 8.69-74.89S321.71 3.241 346.202 5.581c0 0-38.712 64.748-26.071 63.188 12.64-1.56 56.086-67.089 56.086-67.089s63.987-11.703 74.258 25.746c10.27 37.449-6.321 56.173-15.801 58.507-9.481 2.34-70.308 1.56-68.728 9.362 1.58 7.802 42.663 20.285 65.567 13.263 0 0-25.281 61.628-58.456 58.507-33.182-3.121-43.453-23.406-63.987-24.966-20.541-1.56-35.552 3.901-25.282 9.362 10.271 5.462 45.033 1.791 61.617 15.719 16.591 13.928 30.022 22.51-.79 28.752-30.812 6.241-86.108 2.34-97.169-21.065l-11.054-23.399z"/>
            <path fill="#82b378" d="M421.248 39.13S255.345 92.176 236.391 151.469c0 0 .79 26.527-15.801 56.947v11.703s17.381-39.79 26.862-45.251c0 .007-37.133-51.485 173.796-135.738z"/>
            <path fill="#82b378" d="M220.593 208.415s-25.584 38.364-10.999 85.067l6.581-1.744s-6.389-51.437 6.994-77.312c13.39-25.875-2.576-6.011-2.576-6.011z"/>
          </svg>
        </div>
        <div class="topics-wrapper">
          <div class="topics-content left-content">
            <h3 class="headline-5 iv-st-from-bottom"><?= $featured_posts_title ?></h3>
            <ul class="links-wrapper">
              <?php
              $featured_posts = get_field('featured_posts', $blog_id);
              if ($featured_posts): ?>
                <ul>
                  <?php foreach ($featured_posts as $featured_post):
                    $permalink = get_permalink($featured_post);
                    $title = get_the_title($featured_post);
                    ?>
                    <li class="iv-st-from-bottom">
                      <a class="link-hover" href="<?php echo esc_url($permalink); ?>">
                        <?php echo esc_html($title); ?>
                      </a>
                    </li>
                  <?php endforeach; ?>
                </ul>
              <?php endif; ?>

            </ul>
          </div>
          <div class="topics-content right-content">
            <h3 class="headline-5 iv-st-from-bottom"><?= $categories_section_title ?></h3>
            <ul class="links-wrapper">
              <?php
              $categories = get_categories();
              foreach ($categories as $category) { ?>
                <li class="iv-st-from-bottom">
                  <a class="link-hover"
                     href="<?= get_category_link($category->term_id) ?>">
                    <?= $category->name ?>
                  </a>
                </li>
              <?php } ?>
            </ul>
            <button aria-label="open more links" class="see-more-links iv-st-from-bottom">
              <span>See more</span>
              <svg width="19" height="13" viewBox="0 0 19 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M3.84859 1.7304L3.5814 1.4656L3.31421 1.7304L1.73281 3.29762L1.46077 3.56723L1.73281 3.83683L9.23281 11.2696L9.5 11.5344L9.76719 11.2696L17.2672 3.83683L17.5392 3.56722L17.2672 3.29762L15.6858 1.7304L15.4186 1.4656L15.1514 1.7304L9.5 7.33115L3.84859 1.7304Z"
                      fill="#0977E9" stroke="#0977E9" stroke-width="0.759149"/>
              </svg>
            </button>
          </div>
        </div>
      </div>
    </section>

    <section class="featured_resources_block  data-section-class=featured_resources_blocklarge-padding">
      <div class="container">
        <div class="featured-resources-title">
          <h2 class="headline-1 word-up">All Posts in <?php echo single_cat_title(); ?></h2>
        </div>
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">

          <?php
          global $post;
          if (have_posts()) {
            while (have_posts()) {
              the_post();
              $post_id = $post->ID;
              $author_obj = get_author_details($post_id);
              get_template_part("template-parts/card");
            }
          }
          wp_reset_postdata();
          ?>
        </div>
        <!--        <a href="#" class="btn">Load More</a>-->
      </div>
    </section>

    <section class="join_our_newsletter_block" data-section-class="join_our_newsletter_block">
      <div class="container">
        <div class="join-our-newsletter-wrapper">
          <h2 class="headline-1 word-up"><?= $fourth_section_title ?></h2>
          <div class="wysiwyg-block iv-st-from-bottom description">
            <div class="paragraph"><?= $fourth_section_description ?></div>
          </div>
            <div class="iv-st-from-bottom">
          <?= do_shortcode($form_shortcode) ?>
            </div>
        </div>
      </div>
    </section>

  </div>
<?php
get_footer();
