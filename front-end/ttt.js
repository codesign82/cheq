// // import {v4 as uuidV4} from 'uuid';
const {v4, parse} = require('uuid')
let convertGuidToInt = (uuid) => {
  // parse accountId into Uint8Array[16] variable
  let parsedUuid = parse(uuid);
  // console.log(`uuid ${uuid} parsed successfully`);

  // convert to integer - see answers to https://stackoverflow.com/q/39346517/2860309
  let buffer = Buffer.from(parsedUuid);
  // console.log(`parsed uuid converted to buffer`);
  let result = buffer.readUInt32BE(0);
  // console.log(`buffer converted to integer ${result} successfully`);

  return result;
}

// console.log(convertGuidToInt(v4()))
//
//
// var now = new Date();
//
// timestamp = now.getFullYear().toString(); // 2011
// timestamp += (now.getMonth < 9 ? '0' : '') + now.getMonth().toString(); // JS months are 0-based, so +1 and pad with 0's
// timestamp += ((now.getDate < 10) ? '0' : '') + now.getDate().toString();
// pad with a 0
//... etc... with .getHours(), getMinutes(), getSeconds(), getMilliseconds()
const a={}
for (let i = 0; i < 10000000; i++) {
  const b = Date.now() + Math.floor(Math.random()*1000000000);
  // const b = convertGuidToInt(v4());
  if (a[b]) {
    console.log(i,'found')
    break
  }
  i%100000===0&&console.log(i)
  a[b]=true;
}


