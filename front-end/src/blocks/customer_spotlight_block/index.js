import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {Swiper, Pagination, Autoplay} from 'swiper';

Swiper.use([Pagination, Autoplay]);
const customerSpotlightBlock = async (block) => {

  const swiper = new Swiper(block.querySelector('.swiper-container'), {
    loop: true,
    slidesPerView: 1,
    autoplay: {
      delay: 5000,
    },
    pagination: {
      el: block.querySelector('.swiper-pagination'),
      clickable: true,
    },
  });


  const logos = document.querySelectorAll(".company-logo");
  logos[0].classList.add('active');

  swiper.on('slideChange', function (swiper) {


    for (let logo of logos) {
      logo.classList.remove('active')
    }

    logos[swiper.realIndex].classList.add('active');

  });

  animations(block);
  imageLazyLoading(block);
};

export default customerSpotlightBlock;

