import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {homeHeroDesktopSVG} from "../../lazyloaded_svgs/home_hero_desktop";
import {homeHeroMobileSVG} from "../../lazyloaded_svgs/home_hero_mobile";

const homeHeroBlock = async (block) => {

  const lazyloadedSVG = block.querySelector('[lazyloaded-svg="home-hero-desktop"]');
  lazyloadedSVG&&(lazyloadedSVG.innerHTML = homeHeroDesktopSVG);

  const mobileMedia = window.matchMedia('(max-width: 992px)');
  if (mobileMedia.matches){
    const lazyloadedSVG = block.querySelector('[lazyloaded-svg="home-hero-mobile"]');
    lazyloadedSVG&&(lazyloadedSVG.innerHTML = homeHeroMobileSVG);
  }

  animations(block);
    imageLazyLoading(block);
};

export default homeHeroBlock;

