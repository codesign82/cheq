import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";


const downloadReportBlock = async (block) => {

  // add block code here


    animations(block);
    imageLazyLoading(block);
};

export default downloadReportBlock;

