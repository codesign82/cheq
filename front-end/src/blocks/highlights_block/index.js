import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {Swiper, Pagination} from "swiper";

Swiper.use([Pagination]);
const highlightsBlock = async (block) => {

  // add block code here

  const swiperContainer = block.querySelector('.swiper-container');
  const swiper = new Swiper(swiperContainer, {
    slidesPerView: 1,
    spaceBetween: 20,
    pagination: {
      el: swiperContainer.querySelector('.swiper-pagination'),
      clickable: true
    },
    autoplay: {
      delay: 3000,
      disableOnInteraction: false
    }
  })

  animations(block);
  imageLazyLoading(block);
};

export default highlightsBlock;

