import './index.html';
import './style.scss';
import {gsap} from 'gsap';
import {imageLazyLoading} from '../../scripts/functions/imageLazyLoading';
import {animations} from '../../scripts/general/animations';
import {stringToHTML} from '../../scripts/functions/stringToHTML';


export default async (header) => {
  const burgerMenu = header.querySelector('#burger-menu'),
    menu = header.querySelector('.navbar'),
    menuWrapper = menu.querySelector('.navbar-wrapper'),
    menuLinks = menu.querySelectorAll('.menu-item');
  console.log(menuLinks);

  for (let menuLink of menuLinks) {

    menuLink.addEventListener('click', function (e) {
      console.log("a7a");
      if (menuLink.classList.contains('current-menu-item')) return;
      for (let i = 0; i < menuLinks.length; i++) {
        menuLinks[i].classList.remove('current-menu-item');
      }
      menuLink.classList.add('current-menu-item');
    })
  }

  // hide menu
  menu.addEventListener('click', () => {
    menu.classList.remove('active');
    burgerTl.reverse();
    header.classList.remove('active');
    burgerMenu.classList.remove('active');
    // allowPageScroll()
  });
  menuWrapper.addEventListener('click', (e) => {
    e.stopPropagation();
  });
  if (!burgerMenu) return;
  const burgerTl = gsap.timeline({paused: true});
  const burgerSpans = burgerMenu.querySelectorAll('span');
  gsap.set(burgerSpans, {transformOrigin: 'center'});
  burgerTl
    .to(burgerSpans, {
      y: gsap.utils.wrap([`0.6rem`, 0, `-0.6rem`]),
      duration: 0.35,
    })
    .set(burgerSpans, {autoAlpha: gsap.utils.wrap([1, 0, 1])})
    .to(burgerSpans, {rotation: gsap.utils.wrap([45, 0, -45])})
    .set(burgerSpans, {rotation: gsap.utils.wrap([45, 0, 135])});
  burgerMenu.addEventListener('click', function () {
    if (burgerMenu.classList.contains('active')) {
      // allowPageScroll()
      burgerMenu.classList.remove('active');
      menu.classList.remove('active');
      header.classList.remove('active');
      burgerTl.reverse();
    }
    else {
      burgerMenu.classList.add('active');
      menu.classList.add('active');
      header.classList.add('active');

      burgerTl.play();
      // preventPageScroll();
      gsap.fromTo(menu.querySelectorAll('.menu-item '), {
        y: 30,
        autoAlpha: 0,
      }, {
        y: 0,
        autoAlpha: 1,
        stagger: .05,
        duration: .25,
        delay: .5,
      });
    }
  });

  header.classList.toggle('sticky', window.scrollY >= 20);
  window.addEventListener('scroll', function () {
    header.classList.toggle('sticky', window.scrollY >= 20);
  });
  const langSwitchers = header.querySelectorAll('.lang-switcher');
  for (let langSwitcher of langSwitchers) {
    langSwitcher.addEventListener('click', function (e) {
      e.preventDefault();
      window.location.href = window.location.origin + window.location.pathname + langSwitcher.dataset.lang;
    });
  }


  // region open sub menu in responsive
  const mobileMedia = window.matchMedia('(max-width: 992px)');

  const menuItems = header.querySelectorAll('.menu-item-has-children');
  menuItems.forEach((menuItem) => {
    const menuItemBody = menuItem.querySelector('.sub-menu');
    menuItem?.addEventListener('click', (e) => {
      if (e.target!==menuItem) return;
      if (!mobileMedia.matches) return;
      if (!menuItemBody) {
        return;
      }
      const isOpened = menuItem?.classList.toggle('active-page');
      if (!isOpened) {
        gsap.to(menuItemBody, {height: 0});
        menuItem.classList.remove('active');
      }
      else {
        gsap.set(menuItem, {zIndex: 2});
        gsap.to(menuItemBody, {height: 'auto'});
        menuItem.classList.add('active');
      }
    });
  });
  // endregion open sub menu in responsive

  animations(header);
  imageLazyLoading(header);
};

