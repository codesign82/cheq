import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {gsap} from "gsap/gsap-core";

const heroBlock = async (block) => {

  // let svgLines = block.querySelectorAll("svg path"),
  //     svgBox = block.querySelectorAll(".btn-shape"),
  //     lastStep = block.querySelectorAll(".first-step .btn-shape")
  //
  // gsap.from(svgLines, {opacity:0,scale: 0,stagger: 0.1 ,duration: .8, ease:"back", delay:.3, transformOrigin:'left center'})
  // gsap.from(svgBox, {opacity:0,scale: 0,stagger: 0.1 ,duration: .8 ,ease:"back"})

  let tl = gsap.timeline({defaults:{duration: .3, ease: "back"}})
  tl.from(block.querySelector(".animate-1"), {scale: 0})
    .from(block.querySelector(".animate-2"), {scale: 0, transformOrigin:'left center'})
    .from(block.querySelector(".animate-5"), {scale: 0},"<.1")
    .from(block.querySelector(".animate-3"), {scale: 0, transformOrigin:'left center'})
    .from(block.querySelector(".animate-7"), {scale: 0},"<.1")
    .from(block.querySelector(".animate-4"), {scale: 0, transformOrigin:'left center'})
    .from(block.querySelector(".animate-6"), {scale: 0},"<.1")
    .from(block.querySelector(".animate-8"), {scale: 0, transformOrigin:'left center'})
    .from(block.querySelector(".animate-13"), {scale: 0},"<.1")
    .from(block.querySelector(".animate-11"), {scale: 0, transformOrigin:'left center'})
    .from(block.querySelector(".animate-14"), {scale: 0},"<.1")
    .from(block.querySelector(".animate-9"), {scale: 0, transformOrigin:'left center'})
    .from(block.querySelector(".animate-15"), {scale: 0},"<.1")
    .from(block.querySelector(".animate-10"), {scale: 0, transformOrigin:'left center'})
    .from(block.querySelector(".animate-12"), {scale: 0, transformOrigin:'left center'})

  animations(block);
  imageLazyLoading(block);
};

export default heroBlock;

