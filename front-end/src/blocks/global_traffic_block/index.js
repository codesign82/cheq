import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {gsap} from "gsap/gsap-core";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {globalTrafficSVG} from "../../lazyloaded_svgs/global_traffic";

gsap.registerPlugin(ScrollTrigger)
const globalTrafficBlock = async (block) => {
  const lazyloadedSVG = block.querySelector('[lazyloaded-svg="global-traffic"]');
  lazyloadedSVG&&(lazyloadedSVG.innerHTML = globalTrafficSVG);
  let tl = gsap.timeline({paused: true})
  tl
    .from(block.querySelectorAll(".country-path"), {
      scale: 0,
      stagger: 0.03,
      transformOrigin: 'center'
    })
    .from(block.querySelectorAll(".colored-dots path"), {
      scale: 0,
      stagger: {amount: .5},
      transformOrigin: 'center',
      ease: "bounce.out",
      duration: 1
    })


  ScrollTrigger.create({
    trigger: block,
    start: 'top 30%',
    end: 'bottom bottom',
    once: true,
    onEnter: () => tl.play()
  })
  animations(block);
  imageLazyLoading(block);
};

export default globalTrafficBlock;

