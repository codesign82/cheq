import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {gsap} from "gsap";
import {DrawSVGPlugin} from "gsap/DrawSVGPlugin";

const paidmarketingHeroBlock = async (block) => {
  gsap.registerPlugin(DrawSVGPlugin);
  gsap.to(block.querySelector(".svg-animation"), {autoAlpha: 1,duration:.5})
  let tl = gsap.timeline({
    repeat: -1,
    delay:.5,
    yoyo: true,
    repeatDelay: .5,
    defaults: {duration: 2, ease: "elastic.out(1,0.5)"}
  })
  tl
    .from(block.querySelector("#base-2"), {y: 100}, 'same')
    .addLabel('same_glass', '<25%')
    .from(block.querySelector("#base-2 .glass"), {autoAlpha: 0}, 'same_glass')
    .from(block.querySelector("#base-3"), {y: 180}, 'same')
    .from(block.querySelector("#base-3 .glass"), {autoAlpha: 0}, 'same_glass')
    .from(block.querySelector("#base-4"), {y: 240}, 'same')
    .from(block.querySelector("#base-4 .glass"), {autoAlpha: 0}, 'same_glass')
    .from([block.querySelector("#cloud"),block.querySelector("#shape")], {scale:0, transformOrigin:'center', stagger:0.1, ease:'power2.in', duration:.5},'<')
    .fromTo([block.querySelectorAll("#cloud-lines path"),block.querySelectorAll("#shape-lines path")], {drawSVG: '100% 100%'}, {
      drawSVG: '0 100%',
      stagger: {amount: 0.5}
    },'<50%')


  animations(block);
  imageLazyLoading(block);
};

export default paidmarketingHeroBlock;

