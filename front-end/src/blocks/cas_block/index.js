import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {gsap} from "gsap/gsap-core";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {DrawSVGPlugin} from "gsap/DrawSVGPlugin";
import {bubblesSVG} from "../../lazyloaded_svgs/bubbles";

gsap.registerPlugin(ScrollTrigger)
gsap.registerPlugin(DrawSVGPlugin);
const casBlock = async (block) => {
  const lazyloadedSVG = block.querySelector('[lazyloaded-svg="bubbles"]');
  lazyloadedSVG&&(lazyloadedSVG.innerHTML = bubblesSVG);

  let tl = gsap.timeline({paused: true,defaults:{duration:.15}})
  tl
    .from(block.querySelector(".first_circle"), {
      autoAlpha:0,
    })
    .from(block.querySelector(".circle_lines"), {
      autoAlpha:0
    })
    .from(block.querySelector(".circle_lines"), {
      rotate: 90,
      transformOrigin:'center',
      duration:.5,
      repeat:-1,
      repeatDelay:.5,
      ease: "back"
    })
    .from(block.querySelectorAll(".icon_1,.icon_1_wrapper"), {
      scale: 0,
      transformOrigin: 'center'
    },'<')
    .from(block.querySelectorAll(".line_1"),{
      drawSVG: 0,
      stagger: {amount: 0.9}
    })
    .from(block.querySelectorAll(".icon_2"), {
      scale: 0,
      stagger: 0.1,
      transformOrigin: 'center'
    })
    .from(block.querySelectorAll(".line_2"),{
      drawSVG: 0,
      stagger: {amount: 0.9}
    })
    .from(block.querySelectorAll(".icon_3"), {
      scale: 0,
      stagger: 0.1,
      transformOrigin: 'center'
    })
    .from(block.querySelectorAll(".text_2"), {
      scale: 0,
      stagger: 0.1,
      transformOrigin: 'center'
    })
    .from(block.querySelectorAll(".line_3"),{
      drawSVG: 0,
      stagger: {amount: 0.9}
    })
    .from(block.querySelectorAll(".icon_4"), {
      scale: 0,
      stagger: 0.1,
      transformOrigin: 'center'
    })
    .from(block.querySelectorAll(".text_3"), {
      scale: 0,
      stagger: 0.1,
      transformOrigin: 'center'
    })

  ScrollTrigger.create({
    trigger: block,
    start: 'top 30%',
    end: 'bottom bottom',
    once: true,
    onEnter: () => tl.play()
  })


    animations(block);
    imageLazyLoading(block);
};

export default casBlock;

