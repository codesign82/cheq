import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {Swiper, Pagination} from 'swiper';

Swiper.use([Pagination])

const meetTheCheqersBlock = async (block) => {

  // add block code here

  new Swiper(block.querySelector('.swiper-container'), {
    slidesPerView: 1,
    spaceBetween: 40,
    pagination: {
      el: block.querySelector('.swiper-pagination'),
      clickable: true,
    },
    breakpoints: {
      600: {
        slidesPerView: 2,
      },

    }
  })

    animations(block);
    imageLazyLoading(block);
};

export default meetTheCheqersBlock;

