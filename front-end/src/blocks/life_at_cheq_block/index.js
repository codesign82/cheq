import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../scripts/functions/imageLazyLoading';
import {animations} from '../../scripts/general/animations';
import {Pagination, Swiper} from 'swiper';

Swiper.use([Pagination]);

const lifeAtCheqBlock = async (block) => {

  // add block code here

  new Swiper(block.querySelector('.swiper-container'), {
    slidesPerView: 2,
    slidesPerColumn: 2,
    slidesPerColumnFill: 'row',
    spaceBetween: 24,
    pagination: {
      el: block.querySelector('.swiper-pagination'),
      clickable: true,
    },
    breakpoints: {
      600: {
        slidesPerView: 3,
        slidesPerColumn: 1,
        slidesPerGroup: 3,
        spaceBetween: 32,
      },

      992: {
        spaceBetween: 32,
        slidesPerView: 4,
        slidesPerColumn: 1,
        slidesPerGroup: 4,
      },
    },
  });

  animations(block);
  imageLazyLoading(block);
};

export default lifeAtCheqBlock;

