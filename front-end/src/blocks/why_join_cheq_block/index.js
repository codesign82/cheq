import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {gsap} from 'gsap';


const whyJoinCheqBlock = async (block) => {

  // add block code here


  // add block code here
  const faqs = block.querySelectorAll('.faq');
  faqs.forEach((faq) => {
    const faqHead = faq.querySelector('.head');
    const faqBody = faq.querySelector('.body');
    faqHead?.addEventListener('click', (e) => {
      if (!faqBody) {
        return;
      }
      const isOpened = faq?.classList.toggle('active');
      if (!isOpened) {
        gsap.to(faqBody, {height: 0});
      }
      else {
        gsap.to(Array.from(faqs).map(otherFaq => {
          const otherFaqBody = otherFaq.querySelector('.body');
          if (otherFaqBody && faq !== otherFaq) {
            otherFaq?.classList.remove('active');
            gsap.set(otherFaq, {zIndex: 1});
          }
          return otherFaqBody;
        }), {height: 0});
        gsap.set(faq, {zIndex: 2});
        gsap.to(faqBody, {height: 'auto'});
      }
    });
  });



    animations(block);
    imageLazyLoading(block);
};

export default whyJoinCheqBlock;

