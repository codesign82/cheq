import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";


const awardsAndAccoladesBlock = async (block) => {

  // add block code here


    animations(block);
    imageLazyLoading(block);
};

export default awardsAndAccoladesBlock;

