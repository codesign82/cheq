import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";


export default async (footer = document) => {
    animations(footer);
    imageLazyLoading(footer);
};

