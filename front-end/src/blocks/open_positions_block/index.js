import {imageLazyLoading} from '../../scripts/functions/imageLazyLoading';
import {animations} from '../../scripts/general/animations';
import './index.html';
import './style.scss';

const openPositionsBlock = async (block) => {

  let api_url = 'https://www.comeet.co/careers-api/2.0/company/65.005/positions?token=5651AF92B280102F565565205E1594205E';

  fetch(api_url)
    .then(res => res.json())
    .then((jobs_list) => {
      const locations = [];
      for (let jobItem of jobs_list) {
        locations.push(jobItem.location.name)
      }

      function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
      }

      const uniqueLocations = locations.filter(onlyUnique);
      const jobsListWrapper = block.querySelector('.jobs-list-wrapper');
      const locationsWrapper = block.querySelector('.locations-wrapper');

      for (let uniqueLocation of uniqueLocations) {
        locationsWrapper.innerHTML += `<div class="headline-5 tab" data-location-tab="${uniqueLocation}">${uniqueLocation}</div>`;
      }

      for (let jobItem of jobs_list) {
        jobsListWrapper.innerHTML += `
         <div class="col job-item" data-location="${jobItem.location.name}">
           <a target="_blank" data-job-url href="${jobItem.url_comeet_hosted_page}">
             <div class="jop-cards">
               <div class="title-and-arrow">
                 <h3 data-job-department class="headline-4">${jobItem.department}</h3>
                 <svg class="arrow" width="12" height="16" viewBox="0 0 12 16"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg">
                   <path d="M0.666687 16L11.3334 8L0.666686 0L0.666687 16Z"
                         fill="white"/>
                 </svg>
               </div>
               <h4 data-job-title class="headline-3">${jobItem.name}</h4>
               <h5 data-job-location class="headline-5">${jobItem.location.name}</h5>
             </div>
           </a>
         </div>
       `;
      }
      const locationsTabs = block.querySelectorAll('[data-location-tab]');
      for (let locationsTab of locationsTabs) {
        locationsTab.addEventListener('click', function () {
          block.querySelector('.active').classList.remove('active')
          locationsTab.classList.add('active')
          block.scrollIntoView({behavior: 'smooth'});

          const locationAttr = locationsTab.dataset.locationTab;
          const locationAttrVal = `[data-location='${locationAttr}']`;
          for (let job of jobsListWrapper.children) {
            job.classList.add('animate-hide');
          }
          setTimeout(() => {
            for (let job of jobsListWrapper.children) {
              const isMatched = locationAttr === '*' || job.matches(locationAttrVal)
              job.classList.toggle('animate-hide', !isMatched);
              job.classList.toggle('animation-done', !isMatched);
            }
          }, 500)
        });
      }
    })
    .catch(console.log);

  animations(block);
  imageLazyLoading(block);
};

export default openPositionsBlock;

