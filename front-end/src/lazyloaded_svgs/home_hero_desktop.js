export const homeHeroDesktopSVG = `
        <path class="blue-shape animate-2"
              d="M120 28V52C56.1972 52 53.662 129 0 129V105C40.1408 105 52.8169 28 120 28Z"
              fill="url(#shape-gradient)" fill-opacity="0.88"/>
        <path class="back-lines animate-4"
              d="M111 116V140C71 140 45.5 157 4.5 157V133C48 133 53.5 118.5 111 116Z"
              fill="url(#back-line)" fill-opacity="1"/>
        <path class="back-lines animate-10"
              d="M262 39V63C330.056 63 332.761 140 390 140V116C347.183 116 333.662 39 262 39Z"
              fill="url(#back-line)" fill-opacity="1"/>
        <path class="blue-shape animate-3"
              d="M120 223V199C56.1972 199 53.662 122 0 122V146C40.1408 146 52.8169 223 120 223Z"
              fill="url(#shape-gradient)" fill-opacity="0.88"/>
        <path class="red-shape animate-8"
              d="M375.5 0V24C335.5 24 310 41 269 41V17C312.5 17 318 2.5 375.5 0Z"
              fill="url(#red-shape-gradient)" fill-opacity="0.88"/>
        <path class="red-shape animate-9"
              d="M382 24V49.6634C318.197 49.6634 315.662 132 262 132V106.337C302.141 106.337 314.817 24 382 24Z"
              fill="url(#red-shape-gradient)" fill-opacity="0.88"/>
        <path class="back-lines animate-12"
              d="M266 207V231C306 231 331.5 248 372.5 248V224C329 224 323.5 209.5 266 207Z"
              fill="url(#back-line)" fill-opacity="1"/>
        <path class="red-shape animate-11"
              d="M386 126V150C322.197 150 319.662 227 266 227V203C306.141 203 318.817 126 386 126Z"
              fill="url(#red-shape-gradient)" fill-opacity="0.88"/>
        <defs>
          <linearGradient id="shape-gradient" x1="0%" y1="50%" x2="100%" y2="50%" >

            <stop offset="0" stop-color="#67ceff">
              <animate attributeName="offset" values="-.5; 1.0; 1.0;" dur="2.5s" keySplines="3 0 1 1;" repeatCount="indefinite"></animate>
            </stop>

            <stop offset="0.5" stop-color="#FC587B">
              <animate attributeName="offset" values="-.25; 1.25; 1.25;" dur="2.5s" keySplines="3 0 1 1;" repeatCount="indefinite"></animate>
            </stop>

            <stop offset="1" stop-color="#67ceff">
              <animate attributeName="offset" values="0; 1.5; 1.5;" dur="2.5s" keySplines="3 0 1 1;" repeatCount="indefinite"></animate>
            </stop>



          </linearGradient>
          <linearGradient id="back-line" x1="0%" y1="50%" x2="100%" y2="50%" >

            <stop offset="0%" stop-color="#FC587B">
              <animate attributeName="stop-color" values="#7A5FFF; #01FF89; #7A5FFF" dur="4s" repeatCount="indefinite"></animate>
            </stop>

            <stop offset="100%" stop-color="#67ceffc7">
              <animate attributeName="stop-color" values="#FC587B; #7A5FFF; #FC587B" dur="4s" repeatCount="indefinite"></animate>
            </stop>


          </linearGradient>
          <linearGradient id="red-shape-gradient" x1="0%" y1="50%" x2="100%" y2="50%" >

            <stop offset="0" stop-color="#FC587B">
              <animate attributeName="offset" values="-.5; 1.0; 1.0;" begin="1.5s" dur="2.5s" keySplines="3 0 1 1;" repeatCount="indefinite"></animate>
            </stop>

            <stop offset="0.5" stop-color="#8955F7">
              <animate attributeName="offset" values="-.25; 1.25; 1.25;" begin="1.5s" dur="2.5s" keySplines="3 0 1 1;" repeatCount="indefinite"></animate>
            </stop>

            <stop offset="1" stop-color="#FC587B">
              <animate attributeName="offset" values="0; 1.5; 1.5;" begin="1.5s" dur="2.5s" keySplines="3 0 1 1;" repeatCount="indefinite"></animate>
            </stop>



          </linearGradient>
        </defs>
      `;
