export const homeHeroMobileSVG = `
        <path
          d="M63.8126 76.625V90.375C40.9376 90.375 26.3548 100.115 2.90796 100.115V86.3646C27.7845 86.3646 30.9298 78.0573 63.8126 76.625Z"
          fill="url(#paint0_linear)" fill-opacity="0.33"/>
        <path
          d="M206.908 0V13.75C184.033 13.75 169.45 23.4896 146.003 23.4896V9.73958C170.879 9.73958 174.025 1.43229 206.908 0Z"
          fill="url(#paint1_linear)" fill-opacity="0.88"/>
        <path
          d="M152.453 128.76V142.51C175.328 142.51 189.911 152.25 213.358 152.25V138.5C188.481 138.5 185.336 130.193 152.453 128.76Z"
          fill="url(#paint2_linear)" fill-opacity="0.33"/>
        <path
          d="M221.078 82.3542V96.1042C184.591 96.1042 183.141 140.219 152.453 140.219V126.469C175.409 126.469 182.658 82.3542 221.078 82.3542Z"
          fill="url(#paint3_linear)" fill-opacity="0.88"/>
        <path
          d="M68.9595 26.2083V39.9583C32.4722 39.9583 31.0224 84.0729 0.334473 84.0729V70.3229C23.29 70.3229 30.5391 26.2083 68.9595 26.2083Z"
          fill="url(#paint4_linear)" fill-opacity="0.88"/>
        <path
          d="M218.791 23.9167V38.6196C182.303 38.6196 180.853 85.7917 150.166 85.7917V71.0887C173.121 71.0887 180.37 23.9167 218.791 23.9167Z"
          fill="url(#paint5_linear)" fill-opacity="0.88"/>
        <path
          d="M69.625 143.865V130.115C33.1378 130.115 31.6879 86 1 86V99.75C23.9555 99.75 31.2047 143.865 69.625 143.865Z"
          fill="url(#paint6_linear)" fill-opacity="0.88"/>
        <path
          d="M134 25V38.75C172.92 38.75 174.466 82.8646 207.2 82.8646V69.1146C182.714 69.1146 174.982 25 134 25Z"
          fill="url(#paint7_linear)" fill-opacity="0.33"/>
        <defs>
          <linearGradient id="paint0_linear" x1="-4.81236" y1="105.844"
                          x2="63.8126" y2="105.844"
                          gradientUnits="userSpaceOnUse">
            <stop stop-color="#67CEFF"/>
            <stop offset="1" stop-color="#FC587B"/>
          </linearGradient>
          <linearGradient id="paint1_linear" x1="146.003" y1="11.8611"
                          x2="206.908" y2="11.8611"
                          gradientUnits="userSpaceOnUse">
            <stop stop-color="#FC587B"/>
            <stop offset="1" stop-color="#8955F7"/>
          </linearGradient>
          <linearGradient id="paint2_linear" x1="213.358" y1="140.622"
                          x2="152.453" y2="140.622"
                          gradientUnits="userSpaceOnUse">
            <stop stop-color="#8955F7"/>
            <stop offset="1" stop-color="#FC587B"/>
          </linearGradient>
          <linearGradient id="paint3_linear" x1="152.453" y1="111.573"
                          x2="221.078" y2="111.573"
                          gradientUnits="userSpaceOnUse">
            <stop stop-color="#FC587B"/>
            <stop offset="1" stop-color="#8955F7"/>
          </linearGradient>
          <linearGradient id="paint4_linear" x1="0.334472" y1="55.4271"
                          x2="68.9595" y2="55.4271"
                          gradientUnits="userSpaceOnUse">
            <stop offset="0.130208" stop-color="#67EDFF"/>
            <stop offset="1" stop-color="#FC5779"/>
          </linearGradient>
          <linearGradient id="paint5_linear" x1="150.166" y1="55.1605"
                          x2="218.791" y2="55.1605"
                          gradientUnits="userSpaceOnUse">
            <stop stop-color="#FC587B"/>
            <stop offset="1" stop-color="#8955F7"/>
          </linearGradient>
          <linearGradient id="paint6_linear" x1="1" y1="114.646" x2="69.625"
                          y2="114.646" gradientUnits="userSpaceOnUse">
            <stop offset="0.130208" stop-color="#67EDFF"/>
            <stop offset="1" stop-color="#FC5779"/>
          </linearGradient>
          <linearGradient id="paint7_linear" x1="207.2" y1="54.2187" x2="134"
                          y2="54.2187" gradientUnits="userSpaceOnUse">
            <stop stop-color="#8956F7"/>
            <stop offset="1" stop-color="#FC587B"/>
          </linearGradient>
        </defs>
      `;
