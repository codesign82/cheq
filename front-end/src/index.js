import './styles/style.scss';
import {gsap} from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger';
import header from './blocks/header_block';
import hero from './blocks/hero_block';
import {initBlocks} from './blocks';
import {copyLink} from './scripts/general/copy-link';
import {accordionToggle} from './scripts/general/accordion-toggle';
import {breakLine} from './scripts/functions/breakLine';
import {stickySidebar} from './scripts/functions/stickySidebar';
import {getHeightOfViewPort} from './scripts/functions/getHeightOfViewPort';
import {setFormFunctionality} from './scripts/general/set-form-functionality';
import {scrollToHash} from './scripts/general/scroll-to-hash';

const reInvokableFunction = async (container = document) => {
  container.querySelector('header') && await header(container.querySelector('header'));
  await initBlocks(container);
  container.querySelector('.hero_block') && await hero(container.querySelector('.hero_block'));
  setFormFunctionality(container);
  copyLink(container);
  scrollToHash(container);
  accordionToggle(container);
  breakLine(container);
  stickySidebar(container);
  getHeightOfViewPort(container);

  ScrollTrigger.refresh(false);
};
let loaded = false;

async function onLoad() {
  gsap.config({
    nullTargetWarn: false,
  });
  if (document.readyState === 'complete' && !loaded) {
    loaded = true;
    document.body.classList.add('loaded');
    gsap.registerPlugin(ScrollTrigger);

    await reInvokableFunction();
    //region wp-block-columns background position
    const wpCols = document.querySelector('.wp-block-columns');
    if (wpCols) {
      const wpColsStyle = wpCols.style;

      function wpColsStyleFun() {
        const wpColsBgPosition = (window.innerWidth - wpCols.offsetWidth) / 2;
        wpColsStyle.setProperty('--left', '-' + wpColsBgPosition + 'px');
      }

      wpColsStyleFun();
      window.addEventListener('resize', () => {
        wpColsStyleFun();
      })
    }
    //endregion wp-block-columns background position
  }
}

onLoad();

document.onreadystatechange = function () {
  onLoad();
};
