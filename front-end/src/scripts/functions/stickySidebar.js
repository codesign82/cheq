import {gsap} from 'gsap';
import {ScrollTrigger} from 'gsap/ScrollTrigger';

gsap.registerPlugin(ScrollTrigger);

export function stickySidebar(container) {
    const sideBar = container.querySelector('.blog_post_title_block .blog-body .pin-sidebar');
    const post = container.querySelector('.blog_post_title_block .blog-body');
    if (!sideBar) return;
    ScrollTrigger.matchMedia({
        '(min-width:600px)': () => {
            ScrollTrigger.create({
                trigger: sideBar,
                start: "center center",
                endTrigger:'.about-card',
                end: 'top bottom',
                pin: true,
                pinSpacing: false
            })
        }
    });
}