export default function siblings(selector) {
  for (let i = 0; i < selector.length; i++) {
    selector[i].addEventListener('click', function () {
      if (selector[i].classList.contains('active')) {
        selector[i].classList.remove('active');
      } else {
        for (let i = 0; i < selector.length; i++) {
          
          selector[i].classList.remove('active');
        }
        selector[i].classList.add('active');
      }
    });
  }
}