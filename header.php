<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="description" content="<?php if ( is_single() ) {
    single_post_title( '', true );
  } else {
    bloginfo( 'name' );
    echo " - ";
    bloginfo( 'description' );
  } ?>"/>
  <meta
    content="width=device-width, initial-scale=1.0, maximum-scale=5, minimum-scale=1.0"
    name="viewport">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <script>
    const BiggerThanDesignWidth = 1920;
    const designWidth = 1680;
    const desktop = 1680;
    const tablet = 992;
    const mobile = 600;
    const sMobile = 414;
    const resizeHandler = function () {
      if (window.innerWidth >= BiggerThanDesignWidth) {
        document.documentElement.style.fontSize = `${10}px`;
        // document.documentElement.style.fontSize = `${9 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < BiggerThanDesignWidth && window.innerWidth >= desktop) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < desktop && window.innerWidth >= tablet) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / designWidth}px`;
      } else if (window.innerWidth < tablet && window.innerWidth >= mobile) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / tablet}px`;
      } else if (window.innerWidth < mobile && window.innerWidth >= sMobile) {
        document.documentElement.style.fontSize = `${10}px`;
      } else {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / sMobile}px`;
      }
    };
    resizeHandler();
    window.addEventListener('resize', resizeHandler);
  </script>
  <style>
    /* latin */
    @font-face {
      font-family: 'Avenir LT Pro';
      src: url('<?=get_template_directory_uri() .'/fonts/AvenirLTProBlack.woff' ?>') format('woff');
      font-weight: 800;
      font-style: normal;
      font-display: swap;
    }

    @font-face {
      font-family: 'Avenir LT Pro';
      src: url('<?=get_template_directory_uri() .'/fonts/AvenirLTProHeavy.woff' ?>') format('woff');
      font-weight: 700;
      font-style: normal;
      font-display: swap;
    }

    @font-face {
      font-family: 'Avenir LT Pro';
      src: url('<?=get_template_directory_uri() .'/fonts/AvenirLTProRoman.woff' ?>') format('woff');
      font-weight: 400;
      font-style: normal;
      font-display: swap;
    }
  </style>
  <link rel="preload"
        href="<?= get_template_directory_uri() ?>/assets/481.main.css" as="style">
  <link rel="preload"
        href="<?= get_template_directory_uri() ?>/assets/7141.main.css"
        as="style">
  <?php wp_head(); ?>
  <?php
  $code_before_body_tag = get_field( 'code_before_body_tag', 'options' );
  echo $code_before_body_tag;
  ?>
</head>
<!--preloader style-->
<style>
  body:not(.loaded) {
    opacity: 0;
  }

  body {
    transition: opacity .5s;
  }
</style>
<!--end preloader style-->
<!-- ACF Fields -->

<?php
$header_logo_image      = get_field( 'header_logo_image', 'options' );
$header_logo_svg        = get_field( 'header_logo_svg', 'options' );
$is_the_logo_png_or_svg = get_field( 'is_the_logo_png_or_svg', 'options' );
$container_width        = get_field( 'container_width', 'options' );
$cta_button             = get_field( 'cta_button', 'options' );
$is_white_header        = get_field( 'is_white_header', get_the_ID() );
$is_white_header        = $is_white_header ? 'white-header' : '';

?>
<?php if ( $container_width ) { ?>
  <style>
    @media screen and (min-width: 1650px) {
      .container, .wp-block-columns {
        max-width: <?=$container_width?> !important;
      }
    }
  </style>
<?php } ?>

<!-- END ACF -->
<body <?php body_class(); ?> data-barba="wrapper">
<?php
$code_after_body_tag = get_field( 'code_after_body_tag', 'options' );
echo $code_after_body_tag;
?>
<!--  <div class="preloader"></div>-->
<main>
  <!--    header dark or white-->
  <header <?php if ( $is_white_header ) { ?> class="<?= $is_white_header ?>" <?php } ?>>
    <!--      logo  image or svg-->
    <a class="header-logo" aria-label="Home" href="<?= site_url() ?>"
       title="Home">
      <?php if ( $is_the_logo_png_or_svg === 'png' ) { ?>
        <img
          data-src="<?= $header_logo_image['url'] ?>"
          alt="<?= $header_logo_image['alt'] ?>">
      <?php } elseif ( $is_the_logo_png_or_svg === 'svg' ) { ?>
        <div class="header-logo"><?= $header_logo_svg ?></div>
      <?php } ?>
    </a>
    <!--      burger menu and cross-->
    <div class="burger-and-menu">
      <div class="menu">
        <button aria-label="burger" class="burger-menu" id="burger-menu">
          <span></span>
          <span></span>
          <span></span>
        </button>
      </div>
    </div>
    <!--      links-->
    <?php if ( have_rows( 'menu_links', 'options' ) ) : ?>
      <nav class="navbar">
        <div class="navbar-wrapper">
          <!--   all links-->
          <ul class="primary-menu reset-ul">
            <?php while ( have_rows( 'menu_links', 'options' ) ) :
              the_row();
              $menu_link                    = get_sub_field( 'menu_link' );
              $is_has_sub_menu              = get_sub_field( 'is_has_sub_menu' );
              $is_has_sub_menu              = $is_has_sub_menu ? 'menu-item-has-children' :
                '';
              $sub_menu_is_reverse          = get_sub_field( 'sub_menu_is_reverse' );
              $sub_menu_is_reverse          = $sub_menu_is_reverse ? 'reverse' : '';
              $left_title                   = get_sub_field( 'left_title' );
              $right_title                  = get_sub_field( 'right_title' );
              $choose_link_or_logo          = get_sub_field( 'choose_link_or_logo' );
              $choose_content_or_blog_posts = get_sub_field( 'choose_content_or_blog_posts' );
              ?>
              <?php if ( $menu_link ) { ?>
              <li
                class="menu-item <?= $is_has_sub_menu ?>">
                <a title="<?= $menu_link['title'] ?>"
                   href="<?= $menu_link['url'] ?>"
                   target="<?= $menu_link['target'] ?>"><?= $menu_link['title'] ?>
                </a>
                <!--has sub menu-->
                <?php if ( $is_has_sub_menu ) { ?>
                  <div class="arrow">
                    <svg viewBox="0 0 20 14"
                         fill="none">
                      <defs>
                        <linearGradient id="paint0_linear" x1="5.99999"
                                        y1="16"
                                        x2="5.99999" y2="-1.30682e-07"
                                        gradientUnits="userSpaceOnUse">
                          <stop stop-color="#9925CF"/>
                          <stop offset="1" stop-color="#FD0173"/>
                        </linearGradient>
                      </defs>
                      <path class="gradient"
                            d="M0 0.333344L10 13.6667L20 0.333344L0 0.333344Z"
                            fill="url(#paint0_linear)"/>
                      <path class="white"
                            d="M0 0.333344L10 13.6667L20 0.333344L0 0.333344Z"
                            fill="#a0a0a7"/>
                    </svg>
                  </div>
                  <!-- sub menu-->
                  <ul class="sub-menu <?= $sub_menu_is_reverse ?>">
                    <!-- the left content has two repeater links or logos -->
                    <li class="left">
                      <ul class="reset-ul">
                        <!-- left title -->
                        <li
                          class="title">
                          <?php if ( $left_title ) { ?>
                            <?= $left_title ?>
                          <?php } ?>
                        </li>

                        <!--  if select links-->
                        <?php if ( $choose_link_or_logo === 'first_type' ) { ?>
                          <?php
                          if ( have_rows( 'links_text', 'options' ) ) {
                            ?>
                            <?php
                            while ( have_rows( 'links_text', 'options' ) ) {
                              the_row();
                              $link = get_sub_field( 'link' );
                              ?>

                              <?php if ( $link ) { ?>
                                <li
                                  class="menu-item">
                                  <a href="<?= $link['url'] ?>"
                                     target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
                                </li>
                              <?php } ?>

                              <?php
                            } ?>
                            <?php
                          }
                          ?>
                        <?php } ?>

                        <!--if select logos-->
                        <?php if ( $choose_link_or_logo === 'second_type' ) { ?>

                          <?php
                          if ( have_rows( 'logos', 'options' ) ) {
                            ?>
                            <?php
                            while ( have_rows( 'logos', 'options' ) ) {
                              the_row();
                              $logo      = get_sub_field( 'logo' );
                              $logo_link = get_sub_field( 'logo_link' );
                              ?>
                              <?php if ( $logo ) { ?>
                                <li class="menu-item menu-item-logs">
                                  <?php if ( $logo_link ) { ?>
                                  <a class="hover-zoom-child"
                                     href="<?= $logo_link['url'] ?>"
                                     target="<?= $logo_link['target'] ?>"><?php } ?>
                                    <picture class="logo-image">
                                      <img data-src="<?= $logo['url'] ?>"
                                           alt="<?= $logo['alt'] ?>">
                                    </picture>
                                    <?php if ( $logo_link ) { ?></a> <?php } ?>

                                </li>
                              <?php } ?>
                              <?php
                            } ?>
                            <?php
                          }
                          ?>
                        <?php } ?>

                      </ul>
                    </li>
                    <!-- the right content has two repeater content or posts -->
                    <li class="right">
                      <ul class="reset-ul">
                        <!-- right title-->
                        <?php if ( $right_title ) { ?>
                          <li class="title"><?= $right_title ?></li>
                        <?php } ?>

                        <!--  if select links-->
                        <?php if ( $choose_content_or_blog_posts ===
                                   'first_type' ) { ?>
                          <?php
                          if ( have_rows( 'content_cards', 'options' ) ) {
                            while ( have_rows( 'content_cards', 'options' ) ) {
                              the_row();
                              $link_url         = get_sub_field( 'link_url' );
                              $image            = get_sub_field( 'image' );
                              $card_title       = get_sub_field( 'card_title' );
                              $card_description = get_sub_field( 'card_description' );
                              ?>
                              <li class="image-and-text-wrapper">
                                <?php if ( $link_url ) { ?>
                                <a class="hover-zoom-child"
                                   href="<?= $link_url['url'] ?>"
                                   target="<?= $link_url['target'] ?>"><?php } ?>
                                  <picture>
                                    <img data-src="<?= $image['url'] ?>"
                                         alt="<?= $image['alt'] ?>">
                                  </picture>
                                  <?php if ( $link_url ) { ?></a> <?php } ?>

                                <div class="text-wrapper">
                                  <?php if ( $link_url ) { ?>
                                  <a href="<?= $link_url['url'] ?>"
                                     target="<?= $link_url['target'] ?>"><?php } ?>
                                    <h5
                                      class="headline-5 main-title"><?= $card_title ?>
                                    </h5>
                                    <?php if ( $link_url ) { ?></a> <?php } ?>
                                  <?php if ( $card_description ) { ?>
                                    <h5
                                      class="headline-5 description"><?= $card_description ?></h5>
                                  <?php } ?>
                                </div>
                              </li>
                              <?php
                            }
                          }
                          ?>
                        <?php } ?>

                        <!--  if select posts-->
                        <?php if ( $choose_content_or_blog_posts ===
                                   'second_type' ) { ?>
                          <?php

                          $news_posts = get_sub_field( 'select_news_posts' );
                          if ( $news_posts ): ?>
                            <?php foreach ( $news_posts as $post ): // variable must be called $post (IMPORTANT) ?>
                              <?php setup_postdata( $post );
                              $thumbnail_id = get_post_thumbnail_id( $post );
                              $alt          = get_post_meta( $thumbnail_id,
                                '_wp_attachment_image_alt',
                                true );
                              ?>
                              <li class="image-and-text-wrapper"
                                  id="post-id-<?= get_the_ID() ?>">
                                <a class="hover-zoom-child"
                                   href="<?= get_the_permalink( $post ); ?>">
                                  <picture>
                                    <img
                                      data-src="<?= get_the_post_thumbnail_url( $post ) ?>"
                                      alt="<?= $alt ?>"/>
                                  </picture>
                                </a>
                                <div class="text-wrapper">
                                  <a href="<?= get_the_permalink( $post ); ?>"
                                     class="headline-5 main-title"><?= get_the_title( $post ) ?></a>
                                </div>
                              </li>
                            <?php endforeach; ?>
                            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

                          <?php endif; ?>

                        <?php } ?>
                      </ul>
                    </li>
                  </ul>
                <?php } ?>
              </li>
            <?php } ?>
            <?php endwhile; ?>

            <?php if ( $cta_button ) { ?>
              <li
                class="menu-item cta-button">
                <a title="<?= $cta_button['title'] ?>"
                   href="<?= $cta_button['url'] ?>"
                   target="<?= $cta_button['target'] ?>"><?= $cta_button['title'] ?>
                </a>
              </li>
            <?php } ?>
          </ul>
        </div>
      </nav>
    <?php endif; ?>
  </header>
  <div class="page-transition">
