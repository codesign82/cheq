<?php
$post_id = get_the_ID();
$locations = get_the_terms($post_id, 'locations');
$groups = get_the_terms($post_id, 'groups');
?>
<div id="job-id-<?= $post_id; ?>" class="col iv-st-from-bottom-f">
  <a href="<?= get_the_permalink() ?>">
    <div class="jop-cards">
      <div class="title-and-arrow">
        <h3 class="headline-4"><?php
          $count = 0;
          foreach ($groups as $group) {
            echo $count != 0 ? ', ' : '';
            echo $group->name;
            $count++;
          } ?></h3>
        <svg class="arrow" width="12" height="16" viewBox="0 0 12 16"
             fill="none"
             xmlns="http://www.w3.org/2000/svg">
          <path d="M0.666687 16L11.3334 8L0.666686 0L0.666687 16Z"
                fill="white"/>
        </svg>
      </div>
      <h4 class="headline-3"><?= get_the_title() ?></h4>
      <h5 class="headline-5">
        <?php
        $count = 0;
        foreach ($locations as $location) {
          echo $count != 0 ? ', ' : '';
          echo $location->name;
          $count++;
        } ?>
      </h5>
    </div>
  </a>
</div>





