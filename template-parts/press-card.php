<?php
$post_id = get_the_ID();
$date  = get_field('date',$post_id);
$small_description  = get_field('small_description',$post_id);
$press_link  = get_field('press_link',$post_id);
$thumbnail_id  = get_field('image',$post_id)['id'];
$alt           = get_field('image',$post_id)['alt'];
?>

<div id="post-id-<?= $post_id; ?>" class="col iv-st-from-bottom">
  <div class="blog-box">
    <a href="<?=$press_link ?>" target="_blank" class="blog-image aspect-ratio hover-zoom-child">
      <img class="zoom-in" <?php acf_img( $thumbnail_id, '500px', 'medium' ) ?> alt="<?= $alt ?>">
    </a>
    <p class="blog-title headline-4">
      <?=$date ?>
    </p>
    <div class="blog-description paragraph paragraph-16"><?=$small_description ?></div>
    <a href="<?=$press_link ?>" target="_blank" class="read-more">
      <span><?= __( 'Read More', 'cheq' ) ?></span>
      <div class="angle"></div>
    </a>
  </div>
</div>
