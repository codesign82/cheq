<?php
$post_id = get_the_ID();
?>

<div id="case-studies-id-<?=$post_id;?>" class="col iv-st-from-bottom-f">
  <div class="download-card">
    <picture class="aspect-ratio">
      <img data-src="<?=get_the_post_thumbnail_url()?>" alt="card-image">
    </picture>
    <h4 class="headline-4"><?=get_the_title()?></h4>
    <a class="text-and-icon">
      <div class="icon">
        <svg width="16" height="16" viewBox="0 0 16 16" fill="none">
          <path fill-rule="evenodd" clip-rule="evenodd"
                d="M8.61539 0V7.2H11.6923L8 12L4.30769 7.2H7.38462V0H8.61539ZM0 10.6V15.4V16H0.615385H15.3846H16V15.4V10.6H14.7692V14.8H1.23077V10.6H0Z"
                fill="url(#linear)"/>
          <defs>
            <linearGradient id="linear" x1="-1.02445e-07" y1="8"
                            x2="16" y2="8"
                            gradientUnits="userSpaceOnUse">
              <stop stop-color="#9925CF"/>
              <stop offset="1" stop-color="#FD0173"/>
            </linearGradient>
          </defs>
        </svg>
      </div>
      <h5 class="headline-5 text-gradient"><?=__( ' Download report', 'cheq' )?></h5>
    </a>
  </div>
</div>

