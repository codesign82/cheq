<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'download_report_block';
$className = 'download_report_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/download_report_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$image = get_field('image');
$text = get_field('text');
$icon = acf_icon(get_field('icon'));
$download_link = get_field('download_link');
$download_text = get_field('download_text');
$title = get_field('title');
$cta_button = get_field('cta_button');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="download-report-wrapper">
    <div class="left-content">
      <div class="download-card">
        <?php if ($image) { ?>
          <picture class="aspect-ratio iv-st-from-bottom">
            <img data-src="<?=$image['url']?>" alt="<?=$image['alt']?>">
          </picture>
        <?php } ?>
        <?php if ($text) { ?>
          <h4 class="headline-4 word-up"><?=$text?></h4>
        <?php } ?>
        <?php if ($download_link) { ?>
          <a href="<?=$download_link['url']?>"
             target="<?=$download_link['target']?>"
             class="text-and-icon iv-st-from-bottom">
            <?php if ($icon) { ?>
              <div class="icon">
                <?=$icon?>
              </div>
            <?php } ?>
            <?php if ($download_text) { ?>
              <h5 class="headline-5 text-gradient"><?=$download_text?></h5>
            <?php } ?>
          </a>
        <?php } ?>
      </div>
    </div>
    <div class="right-content">
      <?php if ($title) { ?>
        <h2 class="headline-1 iv-st-from-bottom"><?=$title?></h2>
      <?php } ?>
      <?php if ($cta_button) { ?>
        <a href="<?=$cta_button['url']?>" target="<?=$cta_button['target']?>"
           class="btn iv-st-from-bottom"><?=$cta_button['title']?></a>
      <?php } ?>
    </div>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
