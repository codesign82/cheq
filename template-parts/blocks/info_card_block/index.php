<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'info_card_block';
$className = 'info_card_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/info_card_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$profile_image = get_field('profile_image');
$description = get_field('description');
$name = get_field('name');
$job_title = get_field('job_title');
$logo = get_field('logo');
$extra_part_position = get_field('extra_part_position');
$extra_part_color = get_field('extra_part_color');
$extra_part_height = get_field('extra_part_height');


?>

<style>
  <?='#'.$id?>:after{
   background: <?=$extra_part_color?$extra_part_color:'#111222'?>;
   height: <?=$extra_part_height?>% !important;
  <?=$extra_part_position?'top:-0.1rem':'bottom:-0.1rem'?>;

  }
</style>

<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="person-card">
    <?php if ($profile_image) { ?>
      <picture class="big-avatar iv-st-zoom">
        <img data-src="<?=$profile_image['url']?>" alt="<?=$profile_image['alt']?>">
      </picture>
    <?php } ?>
    <div class="person-info">
      <div class="info">
        <?php if ($description) { ?>
          <blockquote class="word-up">
            <span class="quote">“</span>
            <div class="headline-3"><?=$description?></div>
          </blockquote>
        <?php } ?>
        <div class="person-name">
          <div class="name-and-job">
            <?php if ($name) { ?>
              <h4 class="headline-4 iv-st-from-bottom"><?=$name?></h4>
            <?php } ?>
            <?php if ($job_title) { ?>
              <h4
                class="headline-4 iv-st-from-bottom light"><?=$job_title?></h4>
            <?php } ?>
          </div>
          <?php if ($logo) { ?>
            <picture class="card-logo iv-st-from-bottom">
              <img data-src="<?=$logo['url']?>" alt="<?=$logo['alt']?>">
            </picture>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
