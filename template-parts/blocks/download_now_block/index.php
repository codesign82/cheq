<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'download_now_block';
$className = 'download_now_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/download_now_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="download-now-wrapper">
    <?php if ($title) { ?>
      <h2 class="headline-1 gradient-animation iv-st-from-bottom"><?=$title?></h2>
    <?php } ?>
    <?php
    if (have_rows('download_cards')) {
      ?>
      <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
        <?php
        while (have_rows('download_cards')) {
          the_row();
          $link = get_sub_field('link');
          $image = get_sub_field('image');
          $text = get_sub_field('text');
          ?>
          <?php if ($image) { ?>
            <div class="col iv-st-from-bottom hover-image">
              <?php if ($link) { ?>
              <a href="<?=$link['url']?>"
                 target="<?=$link['target']?>"><?php } ?>
                <picture class="aspect-ratio">
                  <img data-src="<?=$image['url']?>"
                       alt="<?=$image['alt']?>">
                </picture>
                <?php if ($link) { ?></a> <?php } ?>
              <?php if ($text) { ?>
                <h3 class="headline-3"><?=$text?></h3>
              <?php } ?>
            </div>
          <?php } ?>
          <?php
        } ?>
      </div>
      <?php
    }
    ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
