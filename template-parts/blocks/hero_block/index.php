<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.

$dark_or_white = get_field( 'dark_or_white' );
$dark_or_white = ! $dark_or_white ? 'dark' : '';
$has_image     = get_field( 'has_image' );
$has_image     = $has_image ? 'has-image' : '';
$dataClass     = 'hero_block';
$className     = 'hero_block ';
$className     .= $dark_or_white . ' ';
$className     .= $has_image;


if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
       '/template-parts/blocks/hero_block/screenshot.png" >';

  return;
endif;
/****************************
 *     Custom ACF Meta      *
 ****************************/

$has_sub_title        = get_field( 'has_sub_title' );
$has_contact_demo     = get_field( 'has_contact_demo' );
$has_logos            = get_field( 'has_logos' );
$max_width            = get_field( 'max_width' );
$main_title           = get_field( 'main_title' );
$sub_title            = get_field( 'sub_title' );
$description          = get_field( 'description' );
$contact_code_snippet = get_field( 'contact_code_snippet' );
$logos_title          = get_field( 'logos_title' );
$hero_image           = get_field( 'hero_image' );

?>
<!-- region CHEQ's Block -->
<?php if ( ! $has_image ) { ?>
  <style>
    @media screen and (min-width: 992px) {
      .hero_block .left-content {
        max-width: <?=$max_width?>% !important;
      }
    }
  </style>
<?php } ?>

<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>

<div class="container">
  <div class="hero-wrapper">
    <div class="left-content iv-st-from-bottom-f">
      <?php if ( $main_title ) { ?>
        <div class="headline-1"><?= $main_title ?></div>
      <?php } ?>
      <?php if ( $has_sub_title ) { ?>
        <h3 class="headline-3"><?= $sub_title ?></h3>
      <?php } ?>
      <?php if ( $description ) { ?>
        <div class="paragraph"><?= $description ?></div>
      <?php } ?>
      <?php if ( $has_contact_demo ) { ?>
        <div class="contact-demo">
          <?php if ( $contact_code_snippet ) {
            echo $contact_code_snippet;
          } ?>
        </div>
      <?php } ?>
    </div>
    <?php if ( $has_image ) { ?>
      <div class="right-content iv-st-from-bottom-f">
        <picture class="aspect-ratio">
          <img data-src="<?= $hero_image['url'] ?>" alt="<?= $hero_image['alt'] ?>">
        </picture>
      </div>
    <?php } ?>
    <?php if ( $has_logos ) { ?>
      <div class="logos iv-st-from-bottom">
        <?php if ( $logos_title ) { ?>
          <h4 class="headline-4"><?= $logos_title ?></h4>
        <?php } ?>
        <ul class="logos-wrapper reset-ul">
          <?php
          if ( have_rows( 'logos' ) ) {
            while ( have_rows( 'logos' ) ) {
              the_row();
              $logo_image = get_sub_field( 'logo_image' );
              $logo_link  = get_sub_field( 'logo_link' );
              ?>
              <?php if ( $logo_image ) { ?>
                <li class="logo iv-st-from-bottom-f hover-image">
                  <?php if ( $logo_link ) { ?>
                  <a href="<?= $logo_link['url'] ?>"
                     target="<?= $logo_link['target'] ?>"><?php } ?>
                    <picture>
                      <img data-src="<?= $logo_image['url'] ?>"
                           alt="<?= $logo_image['alt'] ?>">
                    </picture>
                    <?php if ( $logo_link ) { ?></a> <?php } ?>
                </li>
              <?php } ?>
              <?php
            }
          }
          ?>
        </ul>
      </div>
    <?php } ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
