<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'image_and_text_block';
$className = 'image_and_text_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/image_and_text_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$image = get_field('image');
$title = get_field('title');
$description = get_field('description');
$cta_button = get_field('cta_button');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="image-and-text-wrapper">
    <?php if ($image) { ?>
      <picture class="left-content iv-st-from-bottom-f">
        <img data-src="<?=$image['url']?>" alt="<?=$image['alt']?>">
      </picture>
    <?php } ?>
    <div class="right-content iv-st-from-bottom-f">
      <?php if ($title) { ?>
        <h2 class="headline-1"><?=$title?></h2>
      <?php } ?>
      <?php if ($description) { ?>
        <div class="paragraph"><?=$description?></div>
      <?php } ?>
      <?php if ($cta_button) { ?>
        <a href="<?=$cta_button['url']?>" target="<?=$cta_button['target']?>"
           class="btn">
          <?=$cta_button['title']?>
        </a>
      <?php } ?>
    </div>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
