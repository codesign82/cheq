<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'second_hero_block';
$className = 'second_hero_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/second_hero_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$max_width = get_field('max_width');
$title = get_field('title');
$text = get_field('text');
$description = get_field('description');
?>
<!-- region CHEQ's Block -->

<style>
  @media screen and (min-width: 600px) {
  <?='#'.$id?> .hero-wrapper {
    max-width: <?=$max_width?>% !important;
  }
  }
</style>
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
  <div class="hero-wrapper">
    <?php if ($title) { ?>
      <h2 class="headline-1 iv-st-from-bottom"><?=$title?></h2>
    <?php } ?>
    <?php if ($text) { ?>
      <h3 class="headline-3 word-up"><?=$text?></h3>
    <?php } ?>
    <?php if ($description) { ?>
      <div class="paragraph iv-st-from-bottom"><?=$description?></div>
    <?php } ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
