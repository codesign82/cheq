<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'featured_on_block';
$className = 'featured_on_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/featured_on_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="featured-on-wrapper">
    <?php if ($title) { ?>
      <h2 class="headline-1  iv-st-from-bottom"><?=$title?></h2>
    <?php } ?>
    <?php if ($description) { ?>
      <h3 class="headline-3 word-up "><?=$description?></h3>
    <?php } ?>

    <div class="logos-wrapper">
      <?php
      if (have_rows('logos')) {
        while (have_rows('logos')) {
          the_row();
          $logo_image = get_sub_field('logo_image');
          $logo_link = get_sub_field('logo_link');
          ?>

          <?php if ($logo_image) { ?>
            <div class="logo hover-image iv-st-from-bottom-f">
              <?php if ($logo_link) { ?>
              <a href="<?=$logo_link['url']?>"
                 target="<?=$logo_link['target']?>"><?php } ?>
                <picture class="iv-st-zoom">
                  <img data-src="<?=$logo_image['url']?>"
                       alt="<?=$logo_image['alt']?>">
                </picture>
                <?php if ($logo_link) { ?></a> <?php } ?>
            </div>
          <?php } ?>
          <?php
        }
      }
      ?>
    </div>

  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
