<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'solutions_hero';
$className = 'solutions_hero';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/solutions_hero/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$main_title = get_field('main_title');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <?php if ($title) { ?>
    <h2 class="headline-1 iv-st-from-bottom-f"><?=$title?></h2>
  <?php } ?>
  <div class="row row-cols-1 row-cols-md-2">
    <?php if ($main_title) { ?>
      <div class="col">
        <div class="card main-title iv-st-from-bottom">
          <h2 class="headline-1"><?=$main_title?></h2>
        </div>
      </div>
    <?php } ?>
    <?php
    if (have_rows('solutions_cards')) {
      ?>
      <?php
      while (have_rows('solutions_cards')) {
        the_row();
        $background_color = get_sub_field('background_color');
        $custom_background_color = get_sub_field('custom_background_color');
        $is_custom_background_color = get_sub_field('is_custom_background_color');
        $card_image = get_sub_field('card_image');
        $card_title = get_sub_field('card_title');
        $description = get_sub_field('description');
        $icon = acf_icon(get_sub_field('icon'));

        if ($is_custom_background_color) {
          $background_color = $custom_background_color;
        }
        $background_color = substr($background_color,
                                   strpos($background_color, "linear"));
        ?>

        <div class="col">
          <div class="card iv-st-from-bottom">
            <?php if ($card_image) { ?>
              <picture>
                <img data-src="<?=$card_image['url']?>"
                     alt="<?=$card_image['alt']?>">
              </picture>
            <?php } ?>
            <div class="content"
                 style="background: <?=$background_color?>">
              <?php if ($card_title) { ?>
                <h4 class="headline-4"><?=$card_title?></h4>
              <?php } ?>
              <?php if ($description) { ?>
                <div class="paragraph"><?=$description?></div>
              <?php } ?>
              <?php if ($icon) { ?>
                <div class="arrow">
                  <?=$icon?>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
        <?php
      } ?>
      <?php
    }
    ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
