<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$is_light = get_field('is_light');
$is_light = $is_light ? 'light' : '';

$dataClass = 'cheq_for_marketing_leaders';
$className = 'cheq_for_marketing_leaders ' .  $is_light  ;
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/cheq_for_marketing_leaders/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$text = get_field('text');
$description = get_field('description');
$cta_button = get_field('cta_button');


?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div
    class="cheq-for-marketing-wrapper">
    <?php if ($title) { ?>
      <h2 class="headline-3 gradient-animation text-gradient iv-st-from-bottom"><?=$title?></h2>
    <?php } ?>
    <?php if ($text) { ?>
      <h3 class="headline-1 iv-st-from-bottom"><?=$text?></h3>
    <?php } ?>
    <?php if ($description) { ?>
      <div class="paragraph iv-st-from-bottom"><?=$description?></div>
    <?php } ?>
    <?php if ($cta_button) { ?>
      <a href="<?=$cta_button['url']?>" target="<?=$cta_button['target']?>"
         class="btn iv-st-from-bottom"><?=$cta_button['title']?></a>
    <?php } ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
