<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'marketing_bi_systems_block';
$className = 'marketing_bi_systems_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/marketing_bi_systems_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
$icon = acf_icon(get_field('icon'));
$title = get_field('title');
$description = get_field('description');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="marketing-bi-systems-wrapper">
    <?php if ($main_title) { ?>
      <h2 class="headline-1 iv-st-from-bottom-f"><?=$main_title?></h2>
    <?php } ?>
    <div class="card">
      <div class="top-content">
        <?php if ($icon) { ?>
          <div class="icon-parent">
            <?=$icon?>
          </div>
        <?php } ?>
        <?php if ($title) { ?>
          <h2 class="headline-2 iv-st-from-bottom"><?=$title?></h2>
        <?php } ?>
        <?php if ($description) { ?>
          <h4 class="headline-4 fw-400 iv-st-from-bottom"><?=$description?></h4>
        <?php } ?>
      </div>
      <?php
      if (have_rows('content_wrapper')) {
        ?>
        <?php
        while (have_rows('content_wrapper')) {
          the_row();
          $main_title = get_sub_field('main_title');
          $sub_title = get_sub_field('sub_title');
          $small_text = get_sub_field('small_text');
          $description = get_sub_field('description');
          ?>
          <div class="bottom-content">
            <?php if ($main_title) { ?>
              <h4
                class="headline-4 card-title text-gradient gradient-animation iv-st-from-bottom"><?=$main_title?></h4>
            <?php } ?>
            <?php if ($sub_title) { ?>
              <h2 class="headline-2 iv-st-from-bottom"><?=$sub_title?></h2>
            <?php } ?>
            <div class="headline-4 with-text iv-st-from-bottom"><?=__('With', 'cheq')?></div>
            <?php if ($small_text) { ?>
              <h4
                class="headline-4 text-gradient text-gradient-red small-text iv-st-from-bottom"><?=$small_text?></h4>
            <?php } ?>
            <?php if ($description) { ?>
              <h4
                class="headline-4 fw-400 iv-st-from-bottom"><?=$description?></h4>
            <?php } ?>
          </div>
          <?php
        } ?>
        <?php
      }
      ?>
    </div>
  </div>
</div>
</section>
<!-- endregion CHEQ's Block -->
