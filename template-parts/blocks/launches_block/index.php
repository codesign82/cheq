<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'launches_block';
$className = 'launches_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/launches_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$link = get_field('link');
$image = get_field('image');


$extra_part_position = get_field('extra_part_position');
$extra_part_color = get_field('extra_part_color');
$extra_part_height = get_field('extra_part_height');

?>


<style>
  <?='#'.$id?>:after{
    background: <?=$extra_part_color?$extra_part_color:'#111222'?>;
    height: <?=$extra_part_height?>% !important;
  <?=$extra_part_position?'top:-0.1rem':'bottom:-0.1rem'?>;

  }
</style>

<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="cheq-launches">
    <div class="launches-text">
      <?php if ($title) { ?>
        <h3 class="headline-3 iv-st-from-bottom-f"><?=$title?></h3>
      <?php } ?>
      <?php if ($link) { ?>
        <a class="read-more iv-st-from-bottom-f" href="<?=$link['url']?>"
           target="<?=$link['target']?>">
          <h5 class="headline-5"><?=$link['title']?></h5>
          <svg width="8" height="12" viewBox="0 0 8 12" fill="none"
               xmlns="http://www.w3.org/2000/svg">
            <path d="M0 12L8 6L-5.24537e-07 0L0 12Z" fill="white"/>
          </svg>
        </a>
      <?php } ?>
    </div>
    <?php if ($image) { ?>
      <div class="launches-logo iv-st-from-bottom-f">
        <picture>
          <img
            data-src="<?=$image['url']?>"
            alt="<?=$image['alt']?>">
        </picture>
      </div>
    <?php } ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
