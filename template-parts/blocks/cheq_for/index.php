<?php
// Create id attribute allowing for custom "anchor" value.


$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$is_white = get_field('is_white');
$is_white = $is_white ? 'white' : '';


$dataClass = 'cheq_for';
$className = 'cheq_for ' . $is_white;
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}


if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/cheq_for/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
$launches_title = get_field('launches_title');
$link = get_field('link');
$image = get_field('image');
$description = get_field('description');
$sub_title = get_field('sub_title');
$cta_button = get_field('cta_button');

?>

<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="cheq-for-wrapper">
    <?php if ($main_title) { ?>
      <h2 class="headline-2 text-gradient iv-st-from-bottom"><?=$main_title?></h2>
    <?php } ?>
    <div class="cheq-launches">

      <div class="launches-text">
        <?php if ($launches_title) { ?>
          <h3 class="headline-3 iv-st-from-bottom-f"><?=$launches_title?></h3>
        <?php } ?>
        <?php if ($link) { ?>
          <a class="read-more iv-st-from-bottom-f" href="<?=$link['url']?>"
             target="<?=$link['target']?>">
            <h5 class="headline-5"><?=$link['title']?></h5>
            <svg width="8" height="12" viewBox="0 0 8 12" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
              <path d="M0 12L8 6L-5.24537e-07 0L0 12Z" fill="white"/>
            </svg>
          </a>
        <?php } ?>
      </div>
      <?php if ($image) { ?>
        <div class="launches-logo iv-st-from-bottom-f">
          <picture>
            <img data-src="<?=$image['url']?>"
                 alt="<?=$image['alt']?>">
          </picture>
        </div>
      <?php } ?>
    </div>
    <div class="text-and-button">
      <?php if ($description) { ?>
        <div class="paragraph iv-st-from-bottom">
          <?=$description?>
        </div>
      <?php } ?>
      <?php if ($sub_title) { ?>
        <h3 class="headline-3 word-up"><?=$sub_title?></h3>
      <?php } ?>
      <?php if ($cta_button) { ?>
        <a href="<?=$cta_button['url']?>" target="<?=$cta_button['target']?>"
           class="btn iv-st-from-bottom"><?=$cta_button['title']?></a>
      <?php } ?>
    </div>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
