<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'why_join_cheq_block';
$className = 'why_join_cheq_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/why_join_cheq_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="why-join-cheq-wrapper">
    <?php if ($title) { ?>
      <h2 class="headline-1 gradient-animation iv-st-from-bottom-f"><?=$title?></h2>
    <?php } ?>
    <?php if ($description) { ?>
      <div class="paragraph iv-st-from-bottom-f"><?=$description?></div>
    <?php } ?>

    <?php
    if (have_rows('faqs')) {
      ?>
      <div class="faqs-wrapper">
        <?php
        while (have_rows('faqs')) {
          the_row();
          $question = get_sub_field('question');
          $answer = get_sub_field('answer');
          ?>
          <div class="faq iv-st-from-bottom-f" itemscope itemprop="mainEntity"
               itemtype="https://schema.org/Question">
            <?php if ($question) { ?>
              <div class="head">
                <h4 class="headline-4" itemprop="name"><?=$question?></h4>
                <div class="arrow-right"></div>
              </div>
            <?php } ?>
            <?php if ($answer) { ?>
              <div class="body paragraph" itemscope itemprop="acceptedAnswer"
                   itemtype="https://schema.org/Answer">
                <div class="spacer-top"></div>
                <h2 class="headline-2"><?=$question?></h2>
                <div itemprop="text"><?=$answer?></div>
                <div class="spacer-bottom"></div>
              </div>
            <?php } ?>
          </div>
          <?php
        } ?>
      </div>
      <?php
    }
    ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
