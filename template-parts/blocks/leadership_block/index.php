<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'leadership_block';
$className = 'leadership_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
       '/template-parts/blocks/leadership_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title       = get_field( 'title' );
$description = get_field( 'description' );
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <?php if ( $title ) { ?>
    <div class="headline-1 gradient-animation iv-st-from-bottom"><?= $title ?> </div>
  <?php } ?>
  <?php if ( $description ) { ?>
    <div class="headline-3 iv-st-from-bottom"><?= $description ?></div>
  <?php } ?>


  <?php
  $checkers = get_field( 'checkers' );
  if ( $checkers ): ?>
    <div class="row">
      <?php foreach ( $checkers as $post ):
        setup_postdata( $post );
        $image     = get_field( 'image', $post->ID );
        $job_title = get_field( 'job_title', $post->ID );
        $bio       = get_field( 'bio', $post->ID );
        ?>
        <div class="card-parent col-12 col-md-6 col-lg-4">
          <div class="people-card iv-st-from-bottom-f iv-st-zoom">
            <picture class="big-avatar">
              <img <?php acf_img( $image['id'], '500px', 'medium' ) ?>
                alt="<?= $image['alt'] ?>">
            </picture>
            <div class="card">
              <p class="card-title headline-4 text-gradient-red">
                <?=get_the_title($post->ID) ?>
              </p>
              <?php if ( $job_title ) { ?>
                <p class="paragraph"><?= $job_title ?></p>
              <?php } ?>
              <?php if ( $bio ) { ?>
              <p class="card-sub-tittle"><?=$bio ?></p>
              <?php }?>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
    <?php
    // Reset the global post object so that the rest of the page works correctly.
    wp_reset_postdata(); ?>
  <?php endif; ?>

</div>
</section>

<!-- endregion CHEQ's Block -->
