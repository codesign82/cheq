<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'all_case_studies_block';
$className = 'all_case_studies_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/all_case_studies_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');

$args = array(
  'post_type'      => 'case_studies',
);
$the_query = new WP_Query($args);
$have_posts = $the_query->have_posts();
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="all-case-studies-wrapper">
    <?php if ($title) { ?>
      <h2 class="headline-2  iv-st-from-bottom"><?=$title?></h2>
    <?php } ?>
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
      <?php
      while ($the_query->have_posts()) {
        $the_query->the_post();
        get_template_part("template-parts/case-studies-card");
      }
      /* Restore original Post Data */
      wp_reset_postdata(); ?>
    </div>

  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
