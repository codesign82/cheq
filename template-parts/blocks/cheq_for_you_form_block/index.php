<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'cheq_for_you_form_block';
$className = 'cheq_for_you_form_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
       '/template-parts/blocks/cheq_for_you_form_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title        = get_field( 'main_title' );
$description       = get_field( 'description' );
$form_code_snippet = get_field( 'form_code_snippet' );

?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="cheq-for-you-form-wrapper">
    <?php if ( $main_title ) { ?>
      <h2 class="headline-1 iv-st-from-bottom"><?= $main_title ?></h2>
    <?php } ?>
    <?php if ( $description ) { ?>
      <h3 class="headline-3 iv-st-from-bottom"><?= $description ?></h3>
    <?php } ?>
  </div>
</div>
<?php if ( $form_code_snippet ) { ?>
  <div class="container iv-st-from-bottom">
    <?= $form_code_snippet ?>
  </div>
<?php } ?>
</section>


<!-- endregion CHEQ's Block -->
