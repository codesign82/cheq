<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'our_offices_block';
$className = 'our_offices_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/our_offices_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="our-offices-wrapper">
    <?php if ($main_title) { ?>
      <h2 class="headline-1 iv-st-from-bottom-f"><?=$main_title?></h2>
    <?php } ?>
    <?php
    if (have_rows('locations')) {
      ?>
      <div class="row row-cols-1 row-cols-md-2">
        <?php
        while (have_rows('locations')) {
          the_row();
          $location_link = get_sub_field('location_link');
          $location_title = get_sub_field('location_title');
          $location_description = get_sub_field('location_description');
          $icon = acf_icon(get_sub_field('icon'));
          $link_text = get_sub_field('link_text');

          ?>
          <div class="col iv-st-from-bottom">
            <?php if ($location_title) { ?>
              <h4 class="headline-4"><?=$location_title?></h4>
            <?php } ?>
            <?php if ($location_description) { ?>
              <h4 class="headline-4 fw-400"><?=$location_description?></h4>
            <?php } ?>
            <?php if ($location_link) { ?>
              <a href="<?=$location_link['url']?>"
                 target="<?=$location_link['target']?>" class="location">
                <?php if ($icon) { ?>
                  <div class="icon">
                    <?=$icon?>
                  </div>
                <?php } ?>
                <?php if ($link_text) { ?>
                  <h5 class="headline-5 text-gradient"><?=$link_text?></h5>
                <?php } ?>
              </a>
            <?php } ?>
          </div>
          <?php
        } ?>
      </div>
      <?php
    }
    ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
