<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'offices_block';
$className = 'offices_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/offices_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <?php if ($title) { ?>
    <h2 class="headline-1 gradient-animation iv-st-from-bottom"><?=$title?></h2>
  <?php } ?>
  <?php if ($description) { ?>
    <h3 class="headline-3 word-up"><?=$description?></h3>
  <?php } ?>
  <?php
  if (have_rows('location_cards')) {
    ?>
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
      <?php
      while (have_rows('location_cards')) {
        the_row();
        $location_link = get_sub_field('location_link');
        $location_image = get_sub_field('location_image');
        $location_title = get_sub_field('location_title');
        ?>
        <?php if ($location_image) { ?>
          <div class="col hover-image img-parent iv-st-from-bottom">
            <?php if ($location_link) { ?>
            <a class="location_link" href="<?=$location_link['url']?>"
               target="<?=$location_link['target']?>"><?php } ?>
              <picture class="aspect-ratio">
                <img data-src="<?=$location_image['url']?>"
                     alt="<?=$location_image['alt']?>">
              </picture>
              <?php if ($location_title) { ?>
                <p class="office-country hover-text"><?=$location_title?></p>
              <?php } ?>
              <?php if ($location_link) { ?></a> <?php } ?>
          </div>
        <?php } ?>
        <?php
      } ?>
    </div>
    <?php
  }
  ?>
</div>
</section>

<!-- endregion CHEQ's Block -->
