<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$choose_type_of_block = get_field('choose_type_of_block');
$dataClass = 'platform_hero';
$className = 'platform_hero ' . $choose_type_of_block;
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/platform_hero/screenshot.png" >';

  return;
endif;

if ($choose_type_of_block === 'second_type') {
  $className .= ' cas-hero';
}


/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$contact_text = get_field('contact_text');
$cta_button = get_field('cta_button');
$hero_image = get_field('hero_image');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="hero-wrapper">
    <div class="left-content">
      <?php if ($title) { ?>
        <h2 class="headline-1 gradient-animation"><?=$title?></h2>
      <?php } ?>
      <?php if ($description) { ?>
        <div class="paragraph iv-st-from-bottom"><?=$description?></div>
      <?php } ?>
      <?php if ($contact_text) { ?>
        <h3 class="headline-4 iv-st-from-bottom"><?=$contact_text?></h3>
      <?php } ?>
      <input class="email-input iv-st-from-bottom" type="email"
             placeholder="Work email">
      <?php if ($cta_button) { ?>
        <a class="btn iv-st-from-bottom" href="<?=$cta_button['url']?>"
           target="<?=$cta_button['target']?>"><?=$cta_button['title']?></a>
      <?php } ?>
      <?php if ($choose_type_of_block === 'second_type') { ?>
        <div class="logos">
          <h4 class="headline-4 iv-st-from-bottom">Trusted by over 10,000 customers worldwide</h4>
          <ul class="logos-wrapper reset-ul">
            <?php
            if (have_rows('logos')) {
              while (have_rows('logos')) {
                the_row();
                $logo_image = get_sub_field('logo_image');
                $logo_link = get_sub_field('logo_link');
                ?>
                <?php if ($logo_image) { ?>
                  <li class="logo iv-st-from-bottom-f  hover-image">
                    <?php if ($logo_link) { ?>
                    <a href="<?=$logo_link['url']?>"
                       target="<?=$logo_link['target']?>"><?php } ?>
                      <picture>
                        <img data-src="<?=$logo_image['url']?>"
                             alt="<?=$logo_image['alt']?>">
                      </picture>
                      <?php if ($logo_link) { ?></a> <?php } ?>
                  </li>
                <?php } ?>
                <?php
              }
            }
            ?>
          </ul>
        </div>
      <?php } ?>
    </div>

    <?php if ($choose_type_of_block === 'first_type') { ?>
      <?php if ($hero_image) { ?>
        <div class="right-content iv-st-from-bottom-f">
          <picture class="aspect-ratio">
            <img data-src="<?=$hero_image['url']?>" alt="<?=$hero_image['alt']?>">
          </picture>
        </div>
      <?php } ?>
    <?php } ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
