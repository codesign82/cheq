<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'awards_and_accolades_block';
$className = 'awards_and_accolades_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/awards_and_accolades_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
  <?php if ($title) { ?>
    <h2 class="headline-1 iv-st-from-bottom"><?=$title?></h2>
  <?php } ?>
  <?php if ($description) { ?>
    <h3 class="headline-3 word-up"><?=$description?></h3>
  <?php } ?>

  <div class="awards-and-accolades-wrapper">
    <?php
    if (have_rows('logos')) {
      while (have_rows('logos')) {
        the_row();
        $logo_image = get_sub_field('logo_image');
        $logo_link = get_sub_field('logo_link');
        $text = get_sub_field('text');
        ?>
    <?php if ($logo_image) { ?>
          <div class="logos-wrapper iv-st-from-bottom-f iv-st-zoom hover-image ">
            <?php if ($logo_link) { ?>
            <a class="hover-image" href="<?=$logo_link['url']?>"
               target="<?=$logo_link['target']?>"><?php } ?>
              <picture>
                <img data-src="<?=$logo_image['url']?>"
                     alt="<?=$logo_image['alt']?>">
              </picture>
              <?php if ($logo_link) { ?></a> <?php } ?>
            <h5 class="headline-5"><?=$text?></h5>
          </div>
        <?php } ?>
        <?php
      }
    }
    ?>
  </div>

</div>
</section>

<!-- endregion CHEQ's Block -->
