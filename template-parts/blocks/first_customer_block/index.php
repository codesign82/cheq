<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'first_customer_block';
$className = 'first_customer_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/first_customer_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$cta_button = get_field('cta_button');
$image = get_field('image');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="first-customer-content">
    <div class="img-wrapper circle-bg iv-st-from-bottom-f">
      <?php if ($image) { ?>
        <picture class="aspect-ratio">
          <img
            data-src="<?=$image['url']?>" alt="<?=$image['alt']?>">
        </picture>
      <?php } ?>
    </div>
    <div class="text-content iv-st-from-bottom-f">
      <?php if ($title) { ?>
        <h2 class="headline-1 gradient-animation"><?=$title?></h2>
      <?php } ?>
      <?php if ($description) { ?>
        <div
          class="paragraph paragraph-white"><?=$description?></div>
      <?php } ?>
      <?php if ($cta_button) { ?>
        <a href="<?=$cta_button['url']?>" target="<?=$cta_button['target']?>"
           class="btn"><?=$cta_button['title']?></a>
      <?php } ?>
    </div>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
