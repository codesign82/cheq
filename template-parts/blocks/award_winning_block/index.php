<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'award_winning_block';
$className = 'award_winning_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/award_winning_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <?php if ($title) { ?>
    <h2 class="headline-1 text-gradient-red iv-st-from-bottom"><?=$title?></h2>
  <?php } ?>

  <?php
  if (have_rows('logos')) {
    ?>
    <div class="swiper-container ">
      <div class="swiper-wrapper">
        <?php
        while (have_rows('logos')) {
          the_row();
          $logo_link = get_sub_field('logo_link');
          $logo_image = get_sub_field('logo_image');
          $text = get_sub_field('text');
          ?>
          <div class="swiper-slide">
            <div class="swiper-content iv-st-zoom hover-image">
              <?php if ($logo_link) { ?>
              <a  href="<?=$logo_link['url']?>"
                 target="<?=$logo_link['target']?>"><?php } ?>
                <picture>
                  <img data-src="<?=$logo_image['url']?>"
                       alt="<?=$logo_image['alt']?>">
                </picture>
                <?php if ($logo_link) { ?></a> <?php } ?>
              <h5 class="headline-5"><?=$text?></h5>
            </div>
          </div>
          <?php
        } ?>
      </div>
      <div class="swiper-pagination iv-st-from-bottom-f"></div>
    </div>
    <?php
  }
  ?>

</div>
</section>
<!-- endregion CHEQ's Block -->
