<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'support_block';
$className = 'support_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/support_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">

  <ul class="logos-wrapper reset-ul">
    <?php
    if (have_rows('logos')) {
      while (have_rows('logos')) {
        the_row();
        $logo_image = get_sub_field('logo_image');
        $logo_link = get_sub_field('logo_link');
        ?>
        <?php if ($logo_image) { ?>
          <li class="logo iv-st-from-bottom hover-image">
            <?php if ($logo_link) { ?>
            <a href="<?=$logo_link['url']?>"
               target="<?=$logo_link['target']?>"><?php } ?>
              <picture class="aspect-ratio">
                <img data-src="<?=$logo_image['url']?>"
                     alt="<?=$logo_image['alt']?>">
              </picture>
              <?php if ($logo_link) { ?></a> <?php } ?>
          </li>
        <?php } ?>
        <?php
      }
    }
    ?>
  </ul>

</div>
</section>

<!-- endregion CHEQ's Block -->
