<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'meet_the_cheqers_block';
$className = 'meet_the_cheqers_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
       '/template-parts/blocks/meet_the_cheqers_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field( 'title' );
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="meet-the-cheqers-wrapper">
    <?php if ( $title ) { ?>
      <h2 class="headline-1 iv-st-from-bottom-f"><?= $title ?></h2>
    <?php } ?>
    <?php
    $checkers = get_field( 'checkers' );
    if ( $checkers ) { ?>
      <div class="swiper-container dots">
        <div class="swiper-wrapper">

          <?php foreach ( $checkers as $post ) {
            setup_postdata( $post );
            $image     = get_field( 'image', $post->ID );
            $job_title = get_field( 'job_title', $post->ID );
            $bio       = get_field( 'bio', $post->ID );
            $small_description       = get_field( 'small_description', $post->ID );
            $name       = get_the_title($post->ID);
            ?>
            <div class="swiper-slide">
              <div class="card">
                <?php if ( $image ) { ?>
                  <div class="image-wrapper iv-st-zoom">
                    <picture class="aspect-ratio">
                      <img <?php acf_img( $image['id'], '500px', 'medium' ) ?>
                           alt="<?= $image['alt'] ?>">
                    </picture>
                  </div>
                <?php } ?>
                <?php if ( $name ) { ?>
                  <h4 class="headline-4 iv-st-from-bottom-f"><?= $name ?></h4>
                <?php } ?>
                <?php if ( $job_title ) { ?>
                  <h4
                    class="headline-4 iv-st-from-bottom-f fw-normal"><?= $job_title ?></h4>
                <?php } ?>
                <?php if ( $small_description ) { ?>
                  <div
                    class="paragraph iv-st-from-bottom-f"><?= $small_description ?></div>
                <?php } ?>
              </div>
            </div>
            <?php
          } ?>
        </div>
        <div class="swiper-pagination black-dots"></div>
      </div>
      <?php
    }
    ?>
  </div>
</div>
</section>
<!-- endregion CHEQ's Block -->
