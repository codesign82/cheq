<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'highlights_block';
$className = 'highlights_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/highlights_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field( 'title' );
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <?php if ( $title ) { ?>
    <h2 class="highlights-title headline-2 main-title"><?= $title ?></h2>
  <?php } ?>
  <?php
  $highlights = get_field( 'highlights' );
  if ( $highlights ): ?>
    <div class="highlights swiper-container">
      <div class="swiper-wrapper">
        <?php foreach ( $highlights as $post ): // variable must be called $post (IMPORTANT) ?>
          <?php setup_postdata( $post );
          $thumbnail_id = get_post_thumbnail_id( $post );
          $alt          = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true );

          ?>
          <div class="swiper-slide" id="post-id-<?= get_the_ID() ?>">
            <div class="highlight">
              <a data-swiper-parallax="-100" href="<?= get_the_permalink( $post ); ?>" class="highlight-title headline-2 "><?= get_the_title( $post ) ?></a>
              <div data-swiper-parallax="-200" class="highlight-subtitle paragraph"><?= get_the_excerpt( $post ) ?></div>
              <a data-swiper-parallax="-300" href="<?= get_the_permalink( $post ); ?>" class="read-more">
                <span><?= __( 'Read More', 'cheq' ) ?></span>
                <div class="angle"></div>
              </a>
              <img class="highlight-image parallax-bg" data-swiper-parallax="-23%" data-src="<?= get_the_post_thumbnail_url( $post ) ?>" alt="<?= $alt ?>"/>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
      <div class="swiper-pagination"></div>
    </div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
  <?php endif; ?>
</div>
</section>


<!-- endregion CHEQ's Block -->
