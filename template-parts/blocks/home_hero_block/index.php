<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'home_hero_block';
$className = 'home_hero_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/home_hero_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$has_contact_demo     = get_field( 'has_contact_demo' );
$contact_code_snippet = get_field( 'contact_code_snippet' );
$cta_button = get_field('cta_button');


$first_column = get_field('first_column');
$first_column_button = $first_column['cta_button'];

$second_column = get_field('second_column');
$step_one = $second_column['step_one'];
$step_two = $second_column['step_two'];
$step_three = $second_column['step_three'];

$third_column = get_field('third_column');
$first_cta_button = $third_column['first_cta_button'];
$second_cta_button = $third_column['second_cta_button'];
$third_cta_button = $third_column['third_cta_button'];


?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
  <div class="hero-customers">
    <div class="customers-text iv-st-from-bottom-f">
      <?php if ($title) { ?>
        <h1 class="headline-1 gradient-animation"><?=$title?></h1>
      <?php } ?>
      <?php if ($description) { ?>
        <div
          class="paragraph paragraph-white iv-st-from-bottom-f"><?=$description?></div>
      <?php } ?>

      <?php if ( $has_contact_demo ) { ?>
        <div class="contact-demo">
          <?php if ( $contact_code_snippet ) {
            echo $contact_code_snippet;
          } ?>
        </div>
      <?php } ?>


      <?php if ($cta_button) { ?>
        <a href="<?=$cta_button['url']?>" target="<?=$cta_button['target']?>"
           class="btn iv-st-from-bottom-f"><?=$cta_button['title']?></a>
      <?php } ?>
    </div>
    <div class="headerImg_customer_acquisition circle-bg iv-st-from-bottom-f">

      <?php if ($first_column_button) { ?>
        <div class="first-step">
          <a href="<?=$first_column_button['url']?>" target="<?=$first_column_button['target']?>"
             class="btn-shape animate-1"><?=$first_column_button['title']?></a>
        </div>

      <?php } ?>

      <div class="second-step">
        <?php if ($step_one) { ?>
          <a href="<?=$step_one['url']?>" target="<?=$step_one['target']?>"
             class="btn-shape animate-5"><?=$step_one['title']?></a>
        <?php } ?>


        <?php if ($step_two) { ?>
          <a href="<?=$step_two['url']?>" target="<?=$step_two['target']?>"
             class="btn-shape animate-6"><?=$step_two['title']?></a>
        <?php } ?>


        <?php if ($step_three) { ?>
          <a href="<?=$step_three['url']?>" target="<?=$step_three['target']?>"
             class="btn-shape animate-7"><?=$step_three['title']?></a>
        <?php } ?>

      </div>

      <div class="last-step">
        <?php if ($first_cta_button) { ?>
          <a href="<?=$first_cta_button['url']?>"
             target="<?=$first_cta_button['target']?>"
             class="btn-shape animate-13"><?=$first_cta_button['title']?></a>
        <?php } ?>

        <?php if ($second_cta_button) { ?>
          <a href="<?=$second_cta_button['url']?>"
             target="<?=$second_cta_button['target']?>"
             class="btn-shape animate-14"><?=$second_cta_button['title']?></a>
        <?php } ?>


        <?php if ($third_cta_button) { ?>
          <a href="<?=$third_cta_button['url']?>"
             target="<?=$third_cta_button['target']?>"
             class="btn-shape animate-15"><?=$third_cta_button['title']?></a>
        <?php } ?>

      </div>
      <svg lazyloaded-svg="home-hero-desktop" class="large-screen" width="390" height="248" viewBox="0 0 390 248" fill="none"
           xmlns="http://www.w3.org/2000/svg"></svg>
      <svg lazyloaded-svg="home-hero-mobile" class="small-screen" width="222" height="153" viewBox="0 0 222 153"
           fill="none"></svg>
    </div>
  </div>
  <ul class="logos-wrapper reset-ul">
    <?php
    if (have_rows('logos')) {
      while (have_rows('logos')) {
        the_row();
        $logo_image = get_sub_field('logo_image');
        $logo_link = get_sub_field('logo_link');
        ?>
        <?php if ($logo_image) { ?>
          <li class="logo iv-st-from-bottom-f hover-image">
            <?php if ($logo_link) { ?>
            <a href="<?=$logo_link['url']?>"
               target="<?=$logo_link['target']?>"><?php } ?>
              <picture>
                <img data-src="<?=$logo_image['url']?>"
                     alt="<?=$logo_image['alt']?>">
              </picture>
              <?php if ($logo_link) { ?></a> <?php } ?>
          </li>
        <?php } ?>
        <?php
      }
    }
    ?>
  </ul>
</div>
</section>

<!-- endregion CHEQ's Block -->
