<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'security_platform_block';
$className = 'security_platform_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/security_platform_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="security-platform-wrapper">
    <?php if ($title) { ?>
      <h2 class="headline-1 iv-st-from-bottom gradient-animation"><?=$title?></h2>
    <?php } ?>
    <?php
    if (have_rows('cards')) {
      ?>
      <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
        <?php
        while (have_rows('cards')) {
          the_row();
          $card_title = get_sub_field('card_title');
          $card_image = get_sub_field('card_image');
          $title_one = get_sub_field('title_one');
          $title_two = get_sub_field('title_two');
          $description_one = get_sub_field('description_one');
          $description_two = get_sub_field('description_two');
          $download_title = get_sub_field('download_title');
          $download_text = get_sub_field('download_text');
          $download_image = get_sub_field('download_image');
          $icon = acf_icon(get_sub_field('icon'));
          $cta_button = get_sub_field('cta_button');
          ?>
          <div class="col iv-st-from-bottom">
            <div class="card">
              <?php if ($card_title) { ?>
                <h3 class="headline-3"><?=$card_title?></h3>
              <?php } ?>
              <?php if ($card_image) { ?>
                <picture class="aspect-ratio">
                  <img data-src="<?=$card_image['url']?>"
                       alt="<?=$card_image['alt']?>">
                </picture>
              <?php } ?>
              <?php if ($title_one) { ?>
                <div class="Challenge">
                  <h4 class="headline-4"><?=$title_one?></h4>
                  <div class="paragraph"><?=$description_one?></div>
                </div>
              <?php } ?>
              <?php if ($title_two) { ?>
                <div class="with-cheq">
                  <h4 class="headline-4"><?=$title_two?></h4>
                  <?php if ($description_two) { ?>
                    <div class="paragraph"><?=$description_two?></div>
                  <?php } ?>
                </div>
              <?php } ?>
              <?php if ($download_title) { ?>
                <div class="download">
                  <div class="title-and-icon">
                    <?php if ($download_title) { ?>

                      <h4 class="headline-4"><?=$download_title?></h4>
                    <?php } ?>
                    <?php if ($icon) { ?>
                      <div class="icon">
                        <?=$icon?>
                      </div>
                    <?php } ?>
                  </div>
                  <div class="image-and-title">
                    <?php if ($download_image) { ?>
                      <picture>
                        <img data-src="<?=$download_image['url']?>"
                             alt="<?=$download_image['alt']?>">
                      </picture>
                    <?php } ?>
                    <?php if ($download_text) { ?>
                      <h4 class="headline-4"><?=$download_text?></h4>
                    <?php } ?>
                  </div>
                </div>
              <?php } ?>
              <?php if ($cta_button) { ?>
                <a href="<?=$cta_button['url']?>"
                   target="<?=$cta_button['target']?>" class="btn white">
                  <span><?=$cta_button['title']?></span>
                </a>
              <?php } ?>
            </div>
          </div>
          <?php
        } ?>
      </div>
      <?php
    }
    ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
