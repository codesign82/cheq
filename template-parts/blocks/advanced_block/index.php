<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'advanced_block';
$className = 'advanced_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/advanced_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="advanced-wrapper">

    <?php if ($main_title) { ?>
      <h2 class="headline-1 iv-st-from-bottom"><?=$main_title?></h2>
    <?php } ?>

    <?php
    if (have_rows('steps')) {
      ?>
      <div class="steps ">
        <?php
        while (have_rows('steps')) {
          the_row();
          $title = get_sub_field('title');
          $description = get_sub_field('description');
          ?>
          <div class="step iv-st-from-bottom">
            <?php if ($title) { ?>
              <h2 class="headline-2"><?=$title?></h2>
            <?php } ?>
            <?php if ($description) { ?>
              <div class="paragraph paragraph-white"><?=$description?></div>
            <?php } ?>
          </div>
          <?php
        } ?>
      </div>
      <?php
    }
    ?>

  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
