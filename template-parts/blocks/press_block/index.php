<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'press_block';
$className = 'press_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/press_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$press_title = get_field('press_title');
$posts_per_page = get_field('posts_per_page');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="blogs-title">
    <?php if ( $press_title ) { ?>
      <h2 class="headline-2 iv-st-from-bottom"><?= $press_title ?></h2>
    <?php } ?>
    <div class="filter-by-category-wrapper">
      <select class="filter-by-category" name="filter-by-category">
        <option value="" disabled selected>Filter by Category</option>
        <?php $args = array(
          'orderby' => 'name',
          'order'   => 'ASC',
        );
        foreach ( get_categories( $args ) as $category ) :
          $cat_id = $category->term_id;
          if ( $cat_id == 1 ) {
            continue;
          } // skip 'uncategorized'
          ?>
          <option value="<?= $category->name ?>"><?= $category->name; ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <?php
  $args  = array( 'post_type' => 'press', 'posts_per_page' => $posts_per_page );
  $query = new WP_Query( $args );
  if ( $query->have_posts() ) { ?>
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
      <?php while ( $query->have_posts() ) {
        $query->the_post();
        get_template_part( "template-parts/press-card" );
      } ?>
    </div>
  <?php } ?>
  <div class="load-more-wrapper  <?= $query->max_num_pages <= 1 ? "hidden" : "" ?>"
       data-args='<?= json_encode( $args ) ?>' data-template="template-parts/post-card">
    <button aria-label="Load More Posts" class="btn"><?= __( 'Load More', 'swc' ) ?>
    </button>
    <div class="loader">
      <div class="loader-ball"></div>
      <div class="loader-ball"></div>
      <div class="loader-ball"></div>
    </div>
  </div>
</div>
</section>


<!-- endregion CHEQ's Block -->
