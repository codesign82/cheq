<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'cas_block';
$className = 'cas_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
       '/template-parts/blocks/cas_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title      = get_field( 'title' );
$cta_button = get_field( 'cta_button' );
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="global-traffic-content">
    <div class="text-content">
      <?php if ( $title ) { ?>
        <h2 class="headline-1 gradient-animation"><?= $title ?></h2>
      <?php } ?>
      <?php if ( $cta_button ) { ?>
        <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>"
           class="btn"><?= $cta_button['title'] ?></a>
      <?php } ?>
    </div>
      <picture class="traffic-map">
        <svg lazyloaded-svg="bubbles"  viewBox="0 0 947.2 1086.08"></svg>
      </picture>
  </div>
</div>
</section>
<!-- endregion CHEQ's Block -->
