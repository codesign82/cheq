<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'title_and_descrption_block';
$className = 'title_and_descrption_block circle-bg';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align ' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/title_and_descrption_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$cta_button = get_field('cta_button');
?>
<!-- region CHEQ's Block -->



<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="content">
  <?php if ($title) { ?>
    <h2 class="headline-1 remove-gradient-from-p iv-st-from-bottom"><?=$title?></h2>
  <?php } ?>
  <?php if ($description) { ?>
    <h3 class="headline-3 iv-st-from-bottom"><?=$description?></h3>
  <?php } ?>
  <?php if ($cta_button) { ?>
    <a href="<?=$cta_button['url']?>" target="<?=$cta_button['target']?>"
       class="btn iv-st-from-bottom"><?=$cta_button['title']?></a>
  <?php } ?>
  </div>
</div>
</section>
<!-- endregion CHEQ's Block -->
