<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'why_customer_acquisition_security_matters';
$className = 'why_customer_acquisition_security_matters';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/why_customer_acquisition_security_matters/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="security-matters-wrapper">
    <?php if ($title) { ?>
      <h2 class="headline-1 iv-st-from-bottom"><?=$title?></h2>
    <?php } ?>
    <?php
    if (have_rows('content_wrapper')) {
      ?>
        <?php
        while (have_rows('content_wrapper')) {
          the_row();
          $title = get_sub_field('title');
          $description = get_sub_field('description');
          $link = get_sub_field('link');
          ?>
          <div class="content-wrapper">
            <?php if ($title) { ?>
              <h2 class="headline-2 iv-st-from-bottom"><?=$title?></h2>
            <?php } ?>
            <?php if ($description) { ?>
              <div class="paragraph word-up"><?=$description?></div>
            <?php } ?>
            <?php if ($link) { ?>
              <a href="<?=$link['url']?>" target="<?=$link['target']?>"
                 class="read-more iv-st-from-bottom">
                <span><?=$link['title']?></span>
                <svg width="12" height="16" viewBox="0 0 12 16" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                  <path d="M0.666656 16L11.3333 8L0.666656 0L0.666656 16Z"
                        fill="url(#paint0_linear)"/>
                  <defs>
                    <linearGradient id="paint0_linear" x1="5.99999" y1="16"
                                    x2="5.99999" y2="-1.30682e-07"
                                    gradientUnits="userSpaceOnUse">
                      <stop stop-color="#9925CF"/>
                      <stop offset="1" stop-color="#FD0173"/>
                    </linearGradient>
                  </defs>
                </svg>
              </a>
            <?php } ?>
          </div>
          <?php
        } ?>
      <?php
    }
    ?>

  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
