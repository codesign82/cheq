<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'contact_form_block';
$className = 'contact_form_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
       '/template-parts/blocks/contact_form_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$form_code_snippet = get_field( 'form_code_snippet' );
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<?php if ( $form_code_snippet ) { ?>
  <div class="container iv-st-from-bottom">
    <?= $form_code_snippet ?>
  </div>
<?php } ?>
</section>

<!-- endregion CHEQ's Block -->
