<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'marketing_analytics_block';
$className = 'marketing_analytics_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/marketing_analytics_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title_max_width = get_field('title_max_width');
$title = get_field('title');
$description = get_field('description');
$choose_type_of_block = get_field('choose_type_of_block');
?>

<style>
  @media screen and (min-width: 600px) {
  <?='#'.$id?> .title {
    max-width: <?=$title_max_width?>% !important;
  }
  }
</style>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <?php if ($title) { ?>
    <h2 class="headline-1 title iv-st-from-bottom-f"
    ><?=$title?></h2>
  <?php } ?>
  <?php
  if (have_rows('marketing_cards')) {
    ?>
    <div class="card-wrapper">
      <?php
      while (have_rows('marketing_cards')) {
        the_row();
        $background_color = get_sub_field('background_color');
        //   top content
        $top_icon = acf_icon(get_sub_field('top_icon'));
        $top_title = get_sub_field('top_title');
        $top_description = get_sub_field('top_description');

        //   bottom content
        $bottom_title = get_sub_field('bottom_title');
        $subtitle = get_sub_field('subtitle');
        $small_text = get_sub_field('small_text');
        $bottom_description = get_sub_field('bottom_description');
        ?>
        <div class="card iv-st-zoom"
             <?php if ($background_color){ ?>style="background-color: <?=$background_color?>"<?php } ?>>
          <div class="top">
            <?php if ($top_icon) { ?>
              <div class="icon-parent">
                <?=$top_icon?>
              </div>
            <?php } ?>
            <?php if ($top_title) { ?>
              <h2 class="headline-2 card-title "><?=$top_title?></h2>
            <?php } ?>
            <?php if ($top_description) { ?>
              <div class="paragraph paragraph-white"><?=$top_description?></div>
            <?php } ?>
          </div>
          <?php if ($choose_type_of_block === 'second_type') { ?>
            <div class="bottom">
              <?php if ($bottom_title) { ?>
                <h4
                  class="headline-4  text-gradient gradient-animation"><?=$bottom_title?></h4>
              <?php } ?>
              <?php if ($subtitle) { ?>
                <h2 class="headline-2 card-title "><?=$subtitle?></h2>
              <?php } ?>
              <p class="headline-4 with-text">With</p>
              <?php if ($small_text) { ?>
                <h4
                  class="headline-4 text-gradient text-gradient-red small-text"><?=$small_text?></h4>
              <?php } ?>
              <?php if ($bottom_description) { ?>
                <div
                  class="paragraph paragraph-white"><?=$bottom_description?></div>
              <?php } ?>
            </div>
          <?php } ?>

        </div>
        <?php
      } ?>
    </div>
    <?php
  }
  ?>

</div>

</section>

<!-- endregion CHEQ's Block -->

