<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'become_cheq_partner_block';
$className = 'become_cheq_partner_block circle-bg';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/become_cheq_partner_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
$sub_title = get_field('sub_title');
$description = get_field('description');
$cta_button = get_field('cta_button');
$image_or_icon = get_field('image_or_icon');
$icon = acf_icon(get_field('icon'));
$image = get_field('image');

?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="become-cheq-partner-wrapper">
    <div class="left-content">
      <?php if ($main_title) { ?>
        <h2 class="headline-1 iv-st-from-bottom-f"><?=$main_title?></h2>
      <?php } ?>
      <?php if ($sub_title) { ?>
        <h3 class="headline-3 iv-st-from-bottom-f"><?=$sub_title?></h3>
      <?php } ?>
      <?php if ($description) { ?>
        <div class="paragraph paragraph-white iv-st-from-bottom-f">
          <?=$description?>
        </div>
      <?php } ?>
      <?php if ($cta_button) { ?>
        <a href="<?=$cta_button['url']?>" target="<?=$cta_button['target']?>"
           class="btn iv-st-from-bottom-f"><?=$cta_button['title']?></a>
      <?php } ?>
    </div>
    <div class="right-content iv-st-from-bottom-f">
      <?php if ($image_or_icon === 'icon') { ?>
        <?=$icon?>
      <?php } ?>
      <?php if ($image_or_icon === 'image') { ?>
        <picture>
          <img data-src="<?=$image['url']?>" alt="<?=$image['alt']?>">
        </picture>
      <?php } ?>
    </div>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
