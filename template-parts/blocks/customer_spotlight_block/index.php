<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'customer_spotlight_block';
$className = 'customer_spotlight_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/customer_spotlight_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <?php if ($title) { ?>
    <h2 class="headline-1 iv-st-from-bottom-f"><?=$title?></h2>
  <?php } ?>

  <div class="swiper-parent">
    <?php
    if (have_rows('personal_cards')) {
      ?>
      <div class="left-content iv-st-from-bottom-f">
        <div class="swiper-pagination"></div>
        <?php
        while (have_rows('personal_cards')) {
          the_row();
          $background_color = get_sub_field('background_color');
          $company_logo = get_sub_field('company_logo');
          ?>
          <?php if ($company_logo) { ?>
            <div class="company-logo" style="background-color: <?=$background_color?>" >
            <picture>
              <img data-src="<?=$company_logo['url']?>"
                   alt="<?=$company_logo['alt']?>">
            </picture>
          <?php } ?>
          </div>
          <?php
        } ?>
      </div>
      <?php
    }
    ?>
    <?php
    if (have_rows('personal_cards')) {
      ?>
      <div class="swiper-container iv-st-from-bottom-f">
        <div class="swiper-wrapper">
          <?php
          while (have_rows('personal_cards')) {
            the_row();
            $profile_image = get_sub_field('profile_image');
            $name = get_sub_field('name');
            $job_title = get_sub_field('job_title');
            $description = get_sub_field('description');
            $cta_button = get_sub_field('cta_button');
            ?>
            <div class="swiper-slide">
              <div class="right-content">
                <div class="about">
                  <div class="person">
                    <?php if ($profile_image) { ?>
                      <picture>
                        <img class="small-avatar"
                             data-src="<?=$profile_image['url']?>"
                             alt="<?=$profile_image['alt']?>">
                      </picture>
                    <?php } ?>
                    <?php if ($name) { ?>
                      <div class="about-text">
                        <p class="headline-4"><?=$name?></p>
                        <?php if ($job_title) { ?>
                          <p class="paragraph"><?=$job_title?></p>
                        <?php } ?>
                      </div>
                    <?php } ?>
                  </div>
                  <div class="more-info">
                    <div class="person-info">
                      <div class="quote">“</div>
                      <div class="info">
                        <?php if ($description) { ?>
                          <div class="paragraph"><?=$description?></div>
                        <?php } ?>
                        <?php if ($cta_button) { ?>
                          <a class="btn"
                             href="<?=$cta_button['url']?>"
                             target="<?=$cta_button['target']?>"><?=$cta_button['title']?></a>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php
          } ?>
        </div>
      </div>
      <?php
    }
    ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
