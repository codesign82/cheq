<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'open_positions_block';
$className = 'open_positions_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
       '/template-parts/blocks/open_positions_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field( 'title' );
$description = get_field( 'description' );
//$jobs_list = get_field('jobs_list');

//$args = array(
//  'post_type'      => 'jobs',
//  'posts_per_page' => $jobs_list['posts_per_page'],
//  'order'          => $jobs_list['order'],
//  'paged'          => 1,
//);
//$the_query = new WP_Query($args);
//$have_posts = $the_query->have_posts();
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <?php if ( $title ) { ?>
    <h2 class="headline-1 gradient-animation iv-st-from-bottom-f"><?= $title ?></h2>
  <?php } ?>
  <?php if ( $description ) { ?>
    <div class="paragraph iv-st-from-bottom-f"><?= $description ?></div>
  <?php } ?>

  <div class="filter-wrapper">
    <div class="tabs iv-st-from-bottom-f locations-wrapper">
      <div class="headline-5 tab active" data-location-tab="*">all locations
      </div>
    </div>
  </div>
  <div class="wrapper">
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 jobs-list-wrapper">

    </div>
  </div>
</div>
</section>
<!-- endregion CHEQ's Block -->
