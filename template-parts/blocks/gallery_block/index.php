<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'gallery_block';
$className = 'gallery_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/gallery_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/

$title = get_field('title');
$description = get_field('description');
?>
<!-- region CHEQ's Block -->

<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="gallery-block-wrapper">
    <?php if ($title) { ?>
      <h2 class="headline-1 iv-st-from-bottom"><?=$title?></h2>
    <?php } ?>
    <?php if ($description) { ?>
      <div class="paragraph iv-st-from-bottom">
        <?=$description?>
      </div>
    <?php } ?>
    <?php
    if (have_rows('images')) {
      ?>
      <div class="gallery-wrapper">
        <?php
        while (have_rows('images')) {
          the_row();
          $image = get_sub_field('image');
          $is_big_width = get_sub_field('is_big_width');
          $is_big_width = $is_big_width ? 'big-width' : '';
          ?>
          <?php if ($image) { ?>
            <div class="iv-st-from-bottom picture-wrapper <?php if ($is_big_width) { ?><?=$is_big_width?> <?php } ?>">
              <picture>
              <img data-src="<?=$image['url']?>" alt="<?=$image['alt']?>">
              </picture>
            </div>
          <?php } ?>
          <?php
        } ?>
      </div>
      <?php
    }
    ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
