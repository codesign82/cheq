<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'title_and_emails_blocks';
$className = 'title_and_emails_blocks';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/title_and_emails_blocks/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <?php
    if (have_rows('emails')) {
      ?>
      <div class="content-wrapper ">
        <?php
        while (have_rows('emails')) {
          the_row();
          $title = get_sub_field('title');
          $email_text = get_sub_field('email_text');
          ?>
          <div class="title-and-email iv-st-from-bottom">
            <?php if ($title) { ?>
              <div class="paragraph"><?=$title?></div>
            <?php } ?>
            <?php if ($email_text) { ?>
              <div class="paragraph text-gradient fw-700"><?=$email_text?></div>
            <?php } ?>
          </div>
          <?php
        } ?>
      </div>
      <?php
    }
    ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
