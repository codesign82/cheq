<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'title_and_cards_block';
$className = 'title_and_cards_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/title_and_cards_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
$main_title_color_is_white = get_field('main_title_color_is_white');
$main_title_color_is_white = $main_title_color_is_white ? 'white' : '';
?>


<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="title-and-cards-wrapper">
    <?php if ($main_title) { ?><h2
      class="headline-1 iv-st-from-bottom-f <?php if ($main_title_color_is_white) { ?> <?=$main_title_color_is_white?> <?php } ?>"><?=$main_title?></h2>
    <?php } ?>
    <?php
    if (have_rows('cards')) {
      ?>
      <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
        <?php
        while (have_rows('cards')) {
          the_row();
          $card_background = get_sub_field('card_background');
          $card_main_title = get_sub_field('card_main_title');
          $card_sub_title = get_sub_field('card_sub_title');
          $card_description = get_sub_field('card_description');
          ?>
          <div class="col">
            <div class="card iv-st-from-bottom"
                 style="<?php if ($card_background) { ?> background-color:<?=$card_background?> <?php } ?>">
              <?php if ($card_main_title) { ?>
                <h2 class="headline-4 text-gradient text-gradient-red main-title"><?=$card_main_title?></h2>
              <?php } ?>

              <?php if ($card_sub_title) { ?>
                <h2 class="headline-2"><?=$card_sub_title?></h2>
              <?php } ?>

              <?php if ($card_description) { ?>
                <h4 class="headline-4 fw-bold"><?=$card_description?></h4>
              <?php } ?>
            </div>
          </div>
          <?php
        } ?>
      </div>
      <?php
    }
    ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
