<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'title_and_two_cards_block';
$className = 'title_and_two_cards_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/title_and_two_cards_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="title-and-two-cards-wrapper">
    <?php if ($main_title) { ?>
      <h2 class="headline-1 main-title iv-st-from-bottom-f"><?=$main_title?></h2>
    <?php } ?>
    <?php
    if (have_rows('cards')) {
      ?>
      <div class="cards">
        <?php
        while (have_rows('cards')) {
          the_row();
          $background_color = get_sub_field('background_color');
          $card_image = get_sub_field('card_image');
          $card_title = get_sub_field('card_title');
          $card_sub_title = get_sub_field('card_sub_title');
          $description = get_sub_field('description');
          ?>
          <div class="card iv-st-from-bottom"
               style="
               <?php if ($background_color) {?>
                 background-color:<?=$background_color?>
               <?php }?>">
            <?php if ($card_image) { ?>
              <picture class="aspect-ratio">
                <img data-src="<?=$card_image['url']?>"
                     alt="<?=$card_image['alt']?>">
              </picture>
            <?php } ?>
            <div class="content">

              <?php if ($card_title) { ?>
                <h2 class="headline-1 text-gradient-red"><?=$card_title?></h2>
              <?php } ?>
              <?php if ($card_sub_title) { ?>
                <h2 class="headline-2"><?=$card_sub_title?></h2>
              <?php } ?>
              <?php if ($description) { ?>
                <div class="paragraph paragraph-white"><?=$description?></div>
              <?php } ?>
            </div>
          </div>
          <?php
        } ?>
      </div>
      <?php
    }
    ?>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
