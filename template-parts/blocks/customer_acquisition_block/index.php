<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'customer_acquisition_block';
$className = 'customer_acquisition_block circle-bg';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/customer_acquisition_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$cta_button = get_field('cta_button');
$image = get_field('image');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<
<div class="container">
  <div class="customer-acquisition-wrapper">
    <div class="content">
      <?php if ($title) { ?>
        <h2 class="headline-1 iv-st-from-bottom"><?=$title?></h2>
      <?php } ?>
      <?php if ($cta_button) { ?>
        <a href="<?=$cta_button['url']?>" target="<?=$cta_button['target']?>"
           class="btn iv-st-from-bottom"><?=$cta_button['title']?></a>
      <?php } ?>
    </div>
    <?php if ($image) { ?>
      <div class="image-wrapper iv-st-from-bottom">
        <picture class="aspect-ratio iv-st-from-bottom">
          <img data-src="<?=$image['url']?>" alt="<?=$image['alt']?>">
        </picture>
      </div>
    <?php } ?>
  </div>
</div>
</section>
<!-- endregion CHEQ's Block -->
