<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'life_at_cheq_block';
$className = 'life_at_cheq_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/life_at_cheq_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="life-at-cheq-wrapper">
    <?php if ($title) { ?>
      <h2 class="headline-1 gradient-animation text-gradient-red iv-st-from-bottom"><?=$title?></h2>
    <?php } ?>
    <?php
    if (have_rows('cards')) {
      ?>
      <div class="swiper-container dots">
        <div class="swiper-wrapper">
          <?php
          while (have_rows('cards')) {
            the_row();
            $image = get_sub_field('image');
            ?>
            <div class="swiper-slide">
              <?php if ($image) { ?>
                <picture class="iv-st-from-bottom">
                  <img data-src="<?=$image['url']?>" alt="<?=$image['alt']?>">
                </picture>
              <?php } ?>
            </div>
            <?php
          } ?>
        </div>
        <div class="swiper-pagination"></div>
      </div>
      <?php
    }
    ?>

  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->

