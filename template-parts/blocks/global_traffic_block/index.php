<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'global_traffic_block';
$className = 'global_traffic_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
       '/template-parts/blocks/global_traffic_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title       = get_field( 'title' );
$description = get_field( 'description' );
$cta_button  = get_field( 'cta_button' );
?>
<!-- region CHEQ's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="global-traffic-content">
    <div class="text-content iv-st-from-bottom-f">
      <?php if ( $title ) { ?>
        <h2 class="headline-1 gradient-animation"><?= $title ?></h2>
      <?php } ?>
      <?php if ( $description ) { ?>
        <div class="paragraph">
          <?= $description ?>
        </div>
      <?php } ?>
      <?php if ( $cta_button ) { ?>
        <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>"
           class="btn"><?= $cta_button['title'] ?></a>
      <?php } ?>
    </div>

    <div class="traffic-map">
      <?php if ( false ) { ?>
        <div class="invalid-traffic">
          <!--            <div class="circle-shape">-->
          <!--              <h6 class="progress-number">52%</h6>-->
          <!--              <div class="progress">-->
          <!--                <div class="inside-progress">-->
          <!--                </div>-->
          <!--              </div>-->
          <!--            </div>-->
          <!--            <svg id="Layer_1" data-name="Layer 1"-->
          <!--                 xmlns="http://www.w3.org/2000/svg"-->
          <!--                 xmlns:xlink="http://www.w3.org/1999/xlink"-->
          <!--                 viewBox="0 0 120 120">-->
          <!--              <defs>-->
          <!--                <linearGradient id="linear-gradient" x1="54.15" y1="62.42"-->
          <!--                                x2="106.21" y2="62.42"-->
          <!--                                gradientUnits="userSpaceOnUse">-->
          <!--                  <stop offset="0" stop-color="#9925cf"/>-->
          <!--                  <stop offset="1" stop-color="#fd0173"/>-->
          <!--                </linearGradient>-->
          <!--              </defs>-->
          <!--              <title>progress</title>-->
          <!--              <circle cx="60" cy="60" r="60" fill="#fff"/>-->
          <!--              <path-->
          <!--                d="M106.12,62.12a44,44,0,1,1-44-44A44,44,0,0,1,106.12,62.12ZM34,62.12A28.16,28.16,0,1,0,62.12,34,28.16,28.16,0,0,0,34,62.12Z"-->
          <!--                transform="translate(-2.12 -2.12)" fill="#dddaef"/>-->
          <!--              <circle cx="60" cy="60" stroke-dasharray="250,250"-->
          <!--                      r="36.09" fill="none" stroke-linecap="round"-->
          <!--                      stroke-miterlimit="10" stroke-width="16"-->
          <!--                      stroke="url(#linear-gradient)"/>-->
          <!--              <text class="circ-text" text-anchor="middle" x="50%" y="50%"-->
          <!--                    font-size="12px" fill="red">percentage-->
          <!--              </text>-->
          <!--            </svg>-->
        </div>
      <?php } ?>

      <svg lazyloaded-svg="global-traffic" class="big-img" viewBox="0 0 1024.1 720"></svg>

    </div>
  </div>
</div>
</section>

<!-- endregion CHEQ's Block -->
