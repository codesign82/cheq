<?php
$post_id = get_the_ID();
$thumbnail_id  = get_post_thumbnail_id();
$alt           = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
?>

<div id="post-id-<?= $post_id; ?>" class="col">
  <div class="blog-box">
    <a href="<?php the_permalink(); ?>" class="blog-image aspect-ratio hover-zoom-child">
      <img class="zoom-in" <?php acf_img( $thumbnail_id, '500px', 'medium' ) ?> alt="<?= $alt ?>">
    </a>
    <a href="<?php the_permalink(); ?>" class="blog-title headline-4"><?php the_title() ?></a>
    <div class="blog-description paragraph paragraph-16"><?php the_excerpt(); ?></div>
    <a href="<?php the_permalink(); ?>" class="read-more">
      <span><?= __( 'Read More', 'cheq' ) ?></span>
      <div class="angle"></div>
    </a>
  </div>
</div>
