jQuery(function ($) {
  function niceAccordion(field) {
    const title = field.$el.closest('[data-block').data('title')
    const {template_directory_uri, render_template} = acf.data.blockTypes.filter(block => block.title === title)[0];
    const imageUrl = render_template.replace('index.php', 'screenshot.png');
    field.$el.find('label').first().text(title)
    field.$el.find('.acf-accordion-title').first()
        .css('background-image', `url(${template_directory_uri}/${imageUrl})`)
  }
  
  acf.addAction('load_field/key=field_606707efda9a8', niceAccordion)
  acf.addAction('append_field/key=field_606707efda9a8', niceAccordion)
  
});
