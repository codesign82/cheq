<?php
//region Sets up theme
/**
 * CHEQ functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package CHEQ
 */

if ( ! defined( '_S_VERSION' ) ) {
  // Replace the version number of the theme on each release.
  define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'cheq_setup' ) ) :
  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function cheq_setup() {
    /*
 * Make theme available for translation.
 * Translations can be filed in the /languages/ directory.
 * If you're building a theme based on CHEQ, use a find and replace
 * to change 'CHEQ' to the name of your theme in all the template files.
 */
    load_theme_textdomain( 'cheq', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
    add_theme_support( 'title-tag' );

    /*
 * Enable support for Post Thumbnails on posts and pages.
 *
 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
 */
    add_theme_support( 'post-thumbnails' );

    /*
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
    add_theme_support( 'html5', array(
      'gallery',
      'caption',
    ) );

    register_nav_menus( array(
      'primary-menu' => __( 'Primary Menu' ),
      'top-menu'     => __( 'Top Menu' )
    ) );
  }
endif;
add_action( 'after_setup_theme', 'cheq_setup' );
//endregion Sets up theme

//region ACF.js - Load Custom Gutenberg Style
add_action( 'init', 'add_admin_style_to_post_page_only' );
function add_admin_style_to_post_page_only() {
  global $pagenow;
  if ( 'post.php' == $pagenow || 'post-new.php' == $pagenow ||
       isset( $_GET['post'] ) ) {
    wp_enqueue_script( 'main', get_template_directory_uri() . '/admin-acf.js',
      [ 'jquery' ] );
    wp_enqueue_style( 'admin',
      get_template_directory_uri() . '/assets/admin.css' );
  }
}

//endregion ACF.js - Load Custom Gutenberg Style

//region register blocks

add_action( 'acf/init', 'register_acf_block_types' );
function register_acf_block_types() {
  $developing = true;
  //  $developing = false;

  /* -- blocks will be registered below -- */


  acf_register_block_type( array(
    'name'                   => 'general_classes',
    'title'                  => __( 'General Classes' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/general_classes/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );


  acf_register_block_type( array(
    'name'                   => 'second_hero_block',
    'title'                  => __( 'Second Hero Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/second_hero_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );


  acf_register_block_type( array(
    'name'                   => 'hero_block',
    'title'                  => __( 'Hero Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/hero_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'launches_block',
    'title'                  => __( 'Launches Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/launches_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'global_traffic_block',
    'title'                  => __( 'Global Traffic Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/global_traffic_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'marketing_analytics_block',
    'title'                  => __( 'Marketing Analytics Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/marketing_analytics_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'first_customer_block',
    'title'                  => __( 'First Customer Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/first_customer_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'award_winning_block',
    'title'                  => __( 'Award Winning Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/award_winning_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'info_card_block',
    'title'                  => __( 'Info Card Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/info_card_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'support_block',
    'title'                  => __( 'Support Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/support_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'research_partners_block',
    'title'                  => __( 'Research Partners Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/research_partners_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'featured_on_block',
    'title'                  => __( 'Featured On Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/featured_on_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'customer_acquisition_block',
    'title'                  => __( 'Customer Acquisition Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/customer_acquisition_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'why_join_cheq_block',
    'title'                  => __( 'Why Join Cheq Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/why_join_cheq_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'download_now_block',
    'title'                  => __( 'Download Now Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/download_now_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'cas_block',
    'title'                  => __( 'Cas Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/cas_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'customer_spotlight_block',
    'title'                  => __( 'Customer Spotlight Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/customer_spotlight_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'meet_the_cheqers_block',
    'title'                  => __( 'Meet The Cheqers Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/meet_the_cheqers_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'life_at_cheq_block',
    'title'                  => __( 'Life At Cheq Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/life_at_cheq_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'open_positions_block',
    'title'                  => __( 'Open Positions Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/open_positions_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'gallery_block',
    'title'                  => __( 'Gallery Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/gallery_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'leadership_block',
    'title'                  => __( 'Leadership Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/leadership_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'offices_block',
    'title'                  => __( 'Offices Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/offices_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'title_and_descrption_block',
    'title'                  => __( 'Title And Descrption Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/title_and_descrption_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'awards_and_accolades_block',
    'title'                  => __( 'Awards And Accolades Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/awards_and_accolades_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'platform_hero',
    'title'                  => __( 'Platform Hero' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/platform_hero/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'why_customer_acquisition_security_matters',
    'title'                  => __( 'Why Customer Acquisition Security Matters' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/why_customer_acquisition_security_matters/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'security_platform_block',
    'title'                  => __( 'Security Platform Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/security_platform_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'cheq_for_marketing_leaders',
    'title'                  => __( 'Cheq For Marketing Leaders' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/cheq_for_marketing_leaders/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'solutions_hero',
    'title'                  => __( 'Solutions Hero' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/solutions_hero/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'highlights_block',
    'title'                  => __( 'Highlights Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/highlights_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'blogs_block',
    'title'                  => __( 'Blogs Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/blogs_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'paidmarketing_hero_block',
    'title'                  => __( 'Paidmarketing Hero Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/paidmarketing_hero_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'download_report_block',
    'title'                  => __( 'Download Report Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/download_report_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'all_case_studies_block',
    'title'                  => __( 'All Case Studies Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/all_case_studies_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'cheq_for',
    'title'                  => __( 'Cheq For' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/cheq_for/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'become_cheq_partner_block',
    'title'                  => __( 'Become Cheq Partner Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/become_cheq_partner_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'title_and_cards_block',
    'title'                  => __( 'Title And Cards Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/title_and_cards_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'title_and_two_cards_block',
    'title'                  => __( 'Title And Two Cards Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/title_and_two_cards_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'advanced_block',
    'title'                  => __( 'Advanced Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/advanced_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'home_hero_block',
    'title'                  => __( 'Home Hero Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/home_hero_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'common_types_of_invalid_traffic_block',
    'title'                  => __( 'Common Types Of Invalid Traffic Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/common_types_of_invalid_traffic_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'image_and_text_block',
    'title'                  => __( 'Image And Text Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/image_and_text_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'our_offices_block',
    'title'                  => __( 'Our Offices Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/our_offices_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'marketing_bi_systems_block',
    'title'                  => __( 'Marketing Bi Systems Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/marketing_bi_systems_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'cheq_for_you_form_block',
    'title'                  => __( 'Cheq For You Form Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/cheq_for_you_form_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'contact_form_block',
    'title'                  => __( 'Contact Form Block' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/contact_form_block/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

  acf_register_block_type( array(
    'name'                   => 'title_and_emails_blocks',
    'title'                  => __( 'Title And Emails Blocks' ),
    'template_directory_uri' => get_template_directory_uri(),
    'render_template'        => 'template-parts/blocks/title_and_emails_blocks/index.php',
    'category'               => 'cheq-blocks',
    'icon'                   => 'admin-appearance',
    'supports'               => array( 'anchor' => true ),
    'mode'                   => 'edit',
    'example'                => array(
      'attributes' => array(
        'mode' => 'edit',
        'data' => array( 'is_screenshot' => true ),
      )
    )
  ) );

acf_register_block_type(array(
  'name'                   => 'press_block',
  'title'                  => __('Press Block'),
  'template_directory_uri' => get_template_directory_uri(),
  'render_template'        => 'template-parts/blocks/press_block/index.php',
  'category'               => 'cheq-blocks',
  'icon'                   => 'admin-appearance',
  'supports'               => array('anchor' => true),
  'mode'                   => 'edit',
  'example'                => array(
    'attributes' => array(
      'mode' => 'edit',
      'data' => array('is_screenshot' => true),
    )
  )
));
/* -- blocks will be registered above -- */
}


/*General Settings For Blocks*/
function general_settings_for_blocks( $id, $className, $dataClass ) {
  $group        = get_field( 'section_settings' )['settings'];
  $group_tablet = get_field( 'section_settings' )['settings_tablet']['settings'];
  $group_mobile = get_field( 'section_settings' )['settings_mobile']['settings'];

  $padding_top_mobile      = $group_mobile['padding_top'];
  $padding_left_mobile     = $group_mobile['padding_left'];
  $padding_right_mobile    = $group_mobile['padding_right'];
  $padding_bottom_mobile   = $group_mobile['padding_bottom'];
  $margin_top_mobile       = $group_mobile['margin_top'];
  $margin_left_mobile      = $group_mobile['margin_left'];
  $margin_right_mobile     = $group_mobile['margin_right'];
  $margin_bottom_mobile    = $group_mobile['margin_bottom'];
  $background_image_mobile = $group_mobile['background_image'];
  $background_color_mobile = $group_mobile['background_color'];


  $padding_top_tablet      = $group_tablet['padding_top'];
  $padding_left_tablet     = $group_tablet['padding_left'];
  $padding_right_tablet    = $group_tablet['padding_right'];
  $padding_bottom_tablet   = $group_tablet['padding_bottom'];
  $margin_top_tablet       = $group_tablet['margin_top'];
  $margin_left_tablet      = $group_tablet['margin_left'];
  $margin_right_tablet     = $group_tablet['margin_right'];
  $margin_bottom_tablet    = $group_tablet['margin_bottom'];
  $background_image_tablet = $group_tablet['background_image'];
  $background_color_tablet = $group_tablet['background_color'];


  $padding_top      = $group['padding_top'];
  $padding_left     = $group['padding_left'];
  $padding_right    = $group['padding_right'];
  $padding_bottom   = $group['padding_bottom'];
  $margin_top       = $group['margin_top'];
  $margin_left      = $group['margin_left'];
  $margin_right     = $group['margin_right'];
  $margin_bottom    = $group['margin_bottom'];
  $background_image = $group['background_image'];
  $background_color = $group['background_color'];
  $overlay_color    = $group['overlay_color'];

  $style = $padding_top == 999 ? '' :
    'padding-top:' . $padding_top . 'rem!important;';
  $style .= $padding_right == 999 ? '' :
    'padding-right:' . $padding_right . 'rem!important;';
  $style .= $padding_left == 999 ? '' :
    'padding-left:' . $padding_left . 'rem!important;';
  $style .= $padding_bottom == 999 ? '' :
    'padding-bottom:' . $padding_bottom . 'rem!important;';
  $style .= $margin_top == 999 ? '' :
    'margin-top:' . $margin_top . 'rem!important;';
  $style .= $margin_left == 999 ? '' :
    'margin-left:' . $margin_left . 'rem!important;';
  $style .= $margin_right == 999 ? '' :
    'margin-right:' . $margin_right . 'rem!important;';
  $style .= $margin_bottom == 999 ? '' :
    'margin-bottom:' . $margin_bottom . 'rem!important;';
  $style .= ! $background_color ? '' :
    'background-color:' . $background_color . '!important;';


  $style_tablet = $padding_top_tablet == 999 ? '' :
    'padding-top:' . $padding_top_tablet . 'rem!important;';
  $style_tablet .= $padding_right_tablet == 999 ? '' :
    'padding-right:' . $padding_right_tablet . 'rem!important;';
  $style_tablet .= $padding_left_tablet == 999 ? '' :
    'padding-left:' . $padding_left_tablet . 'rem!important;';
  $style_tablet .= $padding_bottom_tablet == 999 ? '' :
    'padding-bottom:' . $padding_bottom_tablet . 'rem!important;';
  $style_tablet .= $margin_top_tablet == 999 ? '' :
    'margin-top:' . $margin_top_tablet . 'rem!important;';
  $style_tablet .= $margin_left_tablet == 999 ? '' :
    'margin-left:' . $margin_left_tablet . 'rem!important;';
  $style_tablet .= $margin_right_tablet == 999 ? '' :
    'margin-right:' . $margin_right_tablet . 'rem!important;';
  $style_tablet .= $margin_bottom_tablet == 999 ? '' :
    'margin-bottom:' . $margin_bottom_tablet . 'rem!important;';
  $style_tablet .= ! $background_color_tablet ? '' :
    'background-color:' . $background_color_tablet . '!important;';


  $style_mobile = $padding_top_mobile == 999 ? '' :
    'padding-top:' . $padding_top_mobile . 'rem!important;';
  $style_mobile .= $padding_right_mobile == 999 ? '' :
    'padding-right:' . $padding_right_mobile . 'rem!important;';
  $style_mobile .= $padding_left_mobile == 999 ? '' :
    'padding-left:' . $padding_left_mobile . 'rem!important;';
  $style_mobile .= $padding_bottom_mobile == 999 ? '' :
    'padding-bottom:' . $padding_bottom_mobile . 'rem!important;';
  $style_mobile .= $margin_top_mobile == 999 ? '' :
    'margin-top:' . $margin_top_mobile . 'rem!important;';
  $style_mobile .= $margin_left_mobile == 999 ? '' :
    'margin-left:' . $margin_left_mobile . 'rem!important;';
  $style_mobile .= $margin_right_mobile == 999 ? '' :
    'margin-right:' . $margin_right_mobile . 'rem!important;';
  $style_mobile .= $margin_bottom_mobile == 999 ? '' :
    'margin-bottom:' . $margin_bottom_mobile . 'rem!important;';
  $style_mobile .= ! $background_color_mobile ? '' :
    'background-color:' . $background_color_mobile . '!important;';

  $style = ! $style ? '' : '<style>#' . $id . '{' . $style . ';}</style>';
  $style .= ! $background_image ? '' : '<img data-src="' . $background_image['url'] . '" alt="background" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: -1; object-fit: cover;" />';

  $style_tablet = ! $style_tablet ? '' :
    '<style>@media(max-width: 992px){#' . $id . '{' . $style_tablet .
    ';}}</style>';
  $style_tablet .= ! $background_image_tablet ? '' : '<img data-src="' . $background_image_tablet['url'] . '" alt="background" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: -1; object-fit: cover;" />';


  $style_mobile = ! $style_mobile ? '' :
    '<style>@media(max-width: 600px){#' . $id . '{' . $style_mobile .
    ';}}</style>';
  $style_mobile .= ! $background_image_mobile ? '' : '<img data-src="' . $background_image_mobile['url'] . '" alt="background" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: -1; object-fit: cover;" />';

  $overlay_attr = ! $overlay_color ? '' :
    '<style>#' . $id . ':after{content: "";background:' . $overlay_color .
    ';}</style>';

  echo '<section ' . ' id="' . esc_attr( $id ) . '" class="cheq-block ' . ' ' .
       esc_attr( $className ) . '" ' . ' data-section-class="' .
       esc_attr( $dataClass ) . '" >';
  echo $overlay_attr;
  echo $style;
  echo $style_tablet;
  echo $style_mobile;
}


//endregion register blocks

//region wpautop remove filter
function acf_wysiwyg_remove_wpautop() {
  remove_filter( 'acf_the_content', 'wpautop' );
}

//endregion wpautop remove filter

// region Hide regular custom fields metabox
add_filter( 'acf/settings/remove_wp_meta_box', '__return_true' );
// endregion Hide regular custom fields metabox

//region ACF image optimization
add_filter( 'max_srcset_image_width', 'awesome_acf_max_srcset_image_width', 10,
  2 );
function awesome_acf_max_srcset_image_width() {
  return 2200;
}

/**
 * Responsive Image Helper Function
 *
 * @param string $image_id the id of the image (from ACF or similar)
 * @param string $image_size the size of the thumbnail image or custom image size
 * @param string $max_width the max width this image will be shown to build the sizes attribute
 */

function acf_img( $image_id, $max_width, $image_size ) {

  // check the image ID is not blank
  if ( $image_id != '' ) {

    // set the default src image size
    $image_src = wp_get_attachment_image_url( $image_id, $image_size );

    // set the srcset with various image sizes
    $image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

    // generate the markup for the responsive image
    echo 'data-src="' . $image_src . '" srcset="' . $image_srcset .
         '" sizes="(max-width: ' . $max_width . ') 100vw, ' . $max_width . '"';

  }
}

//endregion ACF image optimization

//region Removing Paragraph Tags - ACF shortcode [check]
function my_acf_load_value( $value, $post_id, $field ) {
  $content = apply_filters( 'the_content', $value );
  $content = force_balance_tags( $content );
  $content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
  $content = preg_replace( '~\s?<p>(\s| )+</p>\s?~', '', $content );

  return $content;
}

add_filter( 'acf/load_value/type=wysiwyg', 'my_acf_load_value', 10, 3 );
//endregion Removing Paragraph Tags - ACF shortcode [check]

//region ACF show Options & Settings in Dashboard

// (Optional) Hide the ACF admin menu item.
add_filter( 'acf/settings/show_admin', 'my_acf_settings_show_admin' );
function my_acf_settings_show_admin( $show_admin ) {
  return true;
}

if ( function_exists( 'acf_add_options_page' ) ) {
  acf_add_options_page();
}
//endregion ACF show Options & Settings in Dashboard

//region Make a small-wysiwyg version - use small-field class
add_action( 'admin_head', 'admin_styles' );
function admin_styles() {
  ?>
  <style>
    .small-field .acf-editor-wrap iframe,
    .small-field .acf-editor-wrap.delay .wp-editor-area {
      min-height: 0 !important;
      height: 100px !important;
    }

    .medium-field .acf-editor-wrap iframe,
    .medium-field .acf-editor-wrap.delay .wp-editor-area {
      min-height: 0 !important;
      height: 200px !important;
    }

    .wp-block-freeform.block-library-rich-text__tinymce p {
      min-height: 2vh;
      margin: 0 !important;
    }
  </style>
  <?php
}

//endregion Make a small-wysiwyg version - use small-field class

// region Enqueue scripts and styles.
add_action( 'get_footer', 'cheq_scripts' );
function cheq_scripts() {
  wp_enqueue_style( 'theme-main',
    get_template_directory_uri() . '/assets/main.css', [] );
  wp_enqueue_script( 'theme-main',
    get_template_directory_uri() . '/assets/main.js', '', null,
    true );
}

// endregion Enqueue scripts and styles.

// region Headline shortcode
add_shortcode( 'headline', 'headline_function' );
function headline_function( $atts = array() ) {
  extract( shortcode_atts( array(
    'tag'   => 'h1',
    'class' => 'headline-3',
    'text'  => 'I am the big title',
    'color' => '#000'
  ), $atts ) );

  return "<$tag class='$class' style='color:$color'>$text</$tag>";
}

// endregion Headline shortcode

//region Register Custom Post Types
function custom_post_type() {
  $labels_faqs = array(
    'name'               => _x( 'Faqs', 'Post Type General Name', 'cheq' ),
    'singular_name'      => _x( 'Faq', 'Post Type Singular Name', 'cheq' ),
    'menu_name'          => __( 'Faqs', 'cheq' ),
    'parent_item_colon'  => __( 'Parent Faq', 'cheq' ),
    'all_items'          => __( 'All Faqs', 'cheq' ),
    'view_item'          => __( 'View Faq', 'cheq' ),
    'add_new_item'       => __( 'Add New Faq', 'cheq' ),
    'add_new'            => __( 'Add New', 'cheq' ),
    'edit_item'          => __( 'Edit Faq', 'cheq' ),
    'update_item'        => __( 'Update Faq', 'cheq' ),
    'search_items'       => __( 'Search Faq', 'cheq' ),
    'not_found'          => __( 'Not Found', 'cheq' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'cheq' ),
  );
  $args_faqs   = array(
    'label'               => __( 'faqs', 'cheq' ),
    'description'         => __( 'Faq news and reviews', 'cheq' ),
    'labels'              => $labels_faqs,
    // Features this CPT supports in Post Editor
    'supports'            => array(
      'title',
      'custom-fields',
    ),
    // You can associate this CPT with a taxonomy or custom taxonomy.
    'taxonomies'          => array( 'genres' ),
    /* A hierarchical CPT is like Pages and can have
  * Parent and child items. A non-hierarchical CPT
  * is like Posts.
  */
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'menu_icon'           => 'dashicons-format-quote',
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'show_in_rest'        => true,

  );
  register_post_type( 'faqs', $args_faqs );

  $labels_icons = array(
    'name'               => _x( 'Icons', 'Post Type General Name', 'cheq' ),
    'singular_name'      => _x( 'Icon', 'Post Type Singular Name', 'cheq' ),
    'menu_name'          => __( 'Icons', 'cheq' ),
    'parent_item_colon'  => __( 'Parent Icon', 'cheq' ),
    'all_items'          => __( 'All Icons', 'cheq' ),
    'view_item'          => __( 'View Icon', 'cheq' ),
    'add_new_item'       => __( 'Add New Icon', 'cheq' ),
    'add_new'            => __( 'Add New', 'cheq' ),
    'edit_item'          => __( 'Edit Icon', 'cheq' ),
    'update_item'        => __( 'Update Icon', 'cheq' ),
    'search_items'       => __( 'Search Icon', 'cheq' ),
    'not_found'          => __( 'Not Found', 'cheq' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'cheq' ),
  );
  $args_icons   = array(
    'label'               => __( 'icons', 'cheq' ),
    'description'         => __( 'Icon news and reviews', 'cheq' ),
    'labels'              => $labels_icons,
    // Features this CPT supports in Post Editor
    'supports'            => array(
      'title',
      'custom-fields',
      'thumbnail'
    ),
    // You can associate this CPT with a taxonomy or custom taxonomy.
    'taxonomies'          => array( 'genres' ),
    /* A hierarchical CPT is like Pages and can have
  * Parent and child items. A non-hierarchical CPT
  * is like Posts.
  */
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'menu_icon'           => 'dashicons-visibility',
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'show_in_rest'        => true,

  );
  register_post_type( 'icons', $args_icons );

  $labels_jobs = array(
    'name'               => _x( 'jobs', 'Post Type General Name', 'cheq' ),
    'singular_name'      => _x( 'job', 'Post Type Singular Name', 'cheq' ),
    'menu_name'          => __( 'jobs', 'cheq' ),
    'parent_item_colon'  => __( 'Parent job', 'cheq' ),
    'all_items'          => __( 'All jobs', 'cheq' ),
    'view_item'          => __( 'View job', 'cheq' ),
    'add_new_item'       => __( 'Add New job', 'cheq' ),
    'add_new'            => __( 'Add New', 'cheq' ),
    'edit_item'          => __( 'Edit job', 'cheq' ),
    'update_item'        => __( 'Update job', 'cheq' ),
    'search_items'       => __( 'Search job', 'cheq' ),
    'not_found'          => __( 'Not Found', 'cheq' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'cheq' ),
  );
  $args_jobs   = array(
    'label'               => __( 'jobs', 'cheq' ),
    'description'         => __( 'job news', 'cheq' ),
    'labels'              => $labels_jobs,
    // Features this CPT supports in Post Editor
    'supports'            => array(
      'title',
      'custom-fields',
      'thumbnail',
      'excerpt',
      'editor',
    ),
    // You can associate this CPT with a taxonomy or custom taxonomy.
    'taxonomies'          => array( 'locations' ),
    /* A hierarchical CPT is like Pages and can have
  * Parent and child items. A non-hierarchical CPT
  * is like Posts.
  */
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'menu_icon'           => 'dashicons-list-view',
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'show_in_rest'        => true,

  );
  register_post_type( 'jobs', $args_jobs );

  $labels_checkers = array(
    'name'               => _x( 'checkers', 'Post Type General Name', 'cheq' ),
    'singular_name'      => _x( 'checker', 'Post Type Singular Name', 'cheq' ),
    'menu_name'          => __( 'checkers', 'cheq' ),
    'parent_item_colon'  => __( 'Parent checker', 'cheq' ),
    'all_items'          => __( 'All checkers', 'cheq' ),
    'view_item'          => __( 'View checker', 'cheq' ),
    'add_new_item'       => __( 'Add New checker', 'cheq' ),
    'add_new'            => __( 'Add New', 'cheq' ),
    'edit_item'          => __( 'Edit checker', 'cheq' ),
    'update_item'        => __( 'Update checker', 'cheq' ),
    'search_items'       => __( 'Search checker', 'cheq' ),
    'not_found'          => __( 'Not Found', 'cheq' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'cheq' ),
  );
  $args_checkers   = array(
    'label'               => __( 'checkers', 'cheq' ),
    'description'         => __( 'checker news', 'cheq' ),
    'labels'              => $labels_checkers,
    // Features this CPT supports in Post Editor
    'supports'            => array(
      'title',
      'custom-fields',
    ),
    // You can associate this CPT with a taxonomy or custom taxonomy.
    'taxonomies'          => array( 'locations' ),
    /* A hierarchical CPT is like Pages and can have
  * Parent and child items. A non-hierarchical CPT
  * is like Posts.
  */
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'menu_icon'           => 'dashicons-list-view',
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'show_in_rest'        => true,

  );
  register_post_type( 'checkers', $args_checkers );

  $labels_press = array(
    'name'               => _x( 'press', 'Post Type General Name', 'cheq' ),
    'singular_name'      => _x( 'press', 'Post Type Singular Name', 'cheq' ),
    'menu_name'          => __( 'press', 'cheq' ),
    'parent_item_colon'  => __( 'Parent press', 'cheq' ),
    'all_items'          => __( 'All press', 'cheq' ),
    'view_item'          => __( 'View press', 'cheq' ),
    'add_new_item'       => __( 'Add New press', 'cheq' ),
    'add_new'            => __( 'Add New', 'cheq' ),
    'edit_item'          => __( 'Edit press', 'cheq' ),
    'update_item'        => __( 'Update press', 'cheq' ),
    'search_items'       => __( 'Search press', 'cheq' ),
    'not_found'          => __( 'Not Found', 'cheq' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'cheq' ),
  );
  $args_press   = array(
    'label'               => __( 'press', 'cheq' ),
    'description'         => __( 'press news', 'cheq' ),
    'labels'              => $labels_press,
    // Features this CPT supports in Post Editor
    'supports'            => array(
      'title',
      'custom-fields',
    ),
    // You can associate this CPT with a taxonomy or custom taxonomy.
    'taxonomies'          => array( 'locations' ),
    /* A hierarchical CPT is like Pages and can have
  * Parent and child items. A non-hierarchical CPT
  * is like Posts.
  */
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'menu_icon'           => 'dashicons-list-view',
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'show_in_rest'        => true,
    'rewrite' => array('slug'=>'press-cpt')

  );
  register_post_type( 'press', $args_press );


  $labels_case_studies = array(
    'name'               => _x( 'case_studies', 'Post Type General Name', 'cheq' ),
    'singular_name'      => _x( 'case_studie', 'Post Type Singular Name', 'cheq' ),
    'menu_name'          => __( 'case_studies', 'cheq' ),
    'parent_item_colon'  => __( 'Parent case_studie', 'cheq' ),
    'all_items'          => __( 'All case_studies', 'cheq' ),
    'view_item'          => __( 'View case_studie', 'cheq' ),
    'add_new_item'       => __( 'Add New case_studie', 'cheq' ),
    'add_new'            => __( 'Add New', 'cheq' ),
    'edit_item'          => __( 'Edit case_studie', 'cheq' ),
    'update_item'        => __( 'Update case_studie', 'cheq' ),
    'search_items'       => __( 'Search case_studie', 'cheq' ),
    'not_found'          => __( 'Not Found', 'cheq' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'cheq' ),
  );
  $args_case_studies   = array(
    'label'               => __( 'case_studies', 'cheq' ),
    'description'         => __( 'case_studie news', 'cheq' ),
    'labels'              => $labels_case_studies,
    // Features this CPT supports in Post Editor
    'supports'            => array(
      'title',
      'custom-fields',
      'thumbnail',
    ),
    // You can associate this CPT with a taxonomy or custom taxonomy.
    'taxonomies'          => array( 'locations' ),
    /* A hierarchical CPT is like Pages and can have
  * Parent and child items. A non-hierarchical CPT
  * is like Posts.
  */
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'menu_icon'           => 'dashicons-welcome-learn-more',
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'show_in_rest'        => true,

  );
  register_post_type( 'case_studies', $args_case_studies);


}

add_action( 'init', 'custom_post_type', 0 );
//endregion Register Custom Post Types

//region Register Custom Taxonomy

function custom_taxonomy() {
  $args_jobs = array(
    'labels'       => array(
      'name'          => 'job locations',
      'singular_name' => 'Location',
      'add_new_item'  => 'Add New Location',
      'new_item_name' => "New Location",
    ),
    'public'       => true,
    'hierarchical' => true,
    'show_in_rest' => true,
  );
  register_taxonomy( 'locations', 'jobs', $args_jobs );

  $args_group = array(
    'labels'       => array(
      'name'          => 'job Group',
      'singular_name' => 'Group',
      'add_new_item'  => 'Add New Group',
      'new_item_name' => "New Group",
    ),
    'public'       => true,
    'hierarchical' => true,
    'show_in_rest' => true,
  );
  register_taxonomy( 'groups', 'jobs', $args_group );

}

add_action( 'init', 'custom_taxonomy', 0 );
//endregion Register Custom Taxonomy

//region Function to get Icon From ICON's CPT ( SVG OR IMG )
function acf_icon( $icon_array ) {
  if ( ! $icon_array ) {
    return;
  }
  $icon      = $icon_array[0];
  $icon_type = get_field( 'icon_type', $icon );
  if ( $icon_type === 'svg_code' ) {
    $icon_final = get_field( 'svg_code', $icon );
  } else {
    $icon_file  = get_field( 'file', $icon );
    $icon_final = "<img data-src=" . $icon_file['url'] . " alt=" . $icon_file['alt'] . " />";
  }

  return $icon_final;
}

//endregion Function to get Icon From ICON's CPT ( SVG OR IMG )

//region Function to retrieve Read Time for any Post
function show_read_time( $post_obj ) {
  $post_id      = $post_obj;
  $post_content = get_post( $post_id );
  $content      = $post_content->post_content;
  $mycontent    = do_blocks( $content );
  $word         = str_word_count( strip_tags( $mycontent ) );
  $m            = floor( $word / 150 );
  $est          = $m . ' min' . ( $m == 1 ? '' : 's' );
  echo $est . ' read';
}

//endregion Function to retrieve Read Time for any Post

//region Function to retrieve author details for any Post
function get_author_details( $post_id ) {
  $post_object = get_post( $post_id );
  $author_id   = $post_object->post_author;

  /*get user details*/
  $author_image         = get_field( 'image', 'user_' . $author_id );
  $author_name          = get_field( 'name', 'user_' . $author_id );
  $author_title         = get_field( 'title', 'user_' . $author_id );
  $author_department    = get_field( 'department', 'user_' . $author_id );
  $author_bio           = get_field( 'bio', 'user_' . $author_id );
  $author_twitter_link  = get_field( 'twitter_link', 'user_' . $author_id );
  $author_facebook_link = get_field( 'facebook_link', 'user_' . $author_id );
  $author_linkedin_link = get_field( 'linkedin_link', 'user_' . $author_id );
  $author               = [
    'id'            => $author_id,
    'img'           => $author_image,
    'name'          => $author_name,
    'title'         => $author_title,
    'department'    => $author_department,
    'bio'           => $author_bio,
    'twitter_link'  => $author_twitter_link,
    'facebook_link' => $author_facebook_link,
    'linkedin_link' => $author_linkedin_link,
  ];

  return $author;

}

//endregion Function to retrieve author details for any Post

//region Function to retrieve Categories for any Post
function get_post_categories( $post_id ) {
  $categories = get_the_category( $post_id );
  $count      = 0;
  foreach ( $categories as $i => $category ) {
    $the_link = get_category_link( $category ); ?>
    <a class="small-text blue-text iv-st-from-bottom"
       href="<?php echo esc_html( $the_link ); ?>">
      <?php if ( $count > 0 ) {
        echo '<span>,</span>';
      } ?>
      <?php echo esc_html( $category->name ); ?>
    </a>
    <?php $count ++;
  }
}

//endregion Function to retrieve Categories for any Post

//region get_page_url_by_template_name
function get_page_url_by_template_name( $template_name ) {
  $pages = get_posts( [
    'post_type'   => 'page',
    'post_status' => 'publish',
    'meta_query'  => [
      [
        'key'     => '_wp_page_template',
        'value'   => $template_name . '.php',
        'compare' => '='
      ]
    ]
  ] );
  if ( ! empty( $pages ) ) {
    foreach ( $pages as $pages__value ) {
      return get_permalink( $pages__value->ID );
    }
  }

  return get_bloginfo( 'url' );
}

//endregion get_page_url_by_template_name

//region get thumbnail_url with fallback if there is no-image
function thumbnail_url() {
  $image = get_the_post_thumbnail_url() ? get_the_post_thumbnail_url() :
    get_template_directory_uri() . '/no-image.jpg';

  echo $image;
}

//endregion get thumbnail_url with fallback if there is no-image

//region get small_content
function small_content( $num = 11 ) {
  if ( has_excerpt() ) {
    the_excerpt();
  } else {
    echo wp_trim_words( get_the_content(), $num );
  }
}

//endregion get small_content

//region function for generating an embed link of an FB/Vimeo/Youtube Video
/*to get a valid url, we need to use the below code,
    so the user can add a full video link
    and we will generate the embed url*/
function generateVideoEmbedUrl( $url ) {
  $finalUrl = '';
  if ( strpos( $url, 'vimeo.com/' ) !== false ) {
    //it is Vimeo video
    $videoId = explode( "vimeo.com/", $url )[1];
    if ( strpos( $videoId, '&' ) !== false ) {
      $videoId = explode( "&", $videoId )[0];
    }
    $finalUrl .= 'https://player.vimeo.com/video/' . $videoId;
  } else {
    if ( strpos( $url, 'youtube.com/' ) !== false ) {
      //it is Youtube video
      $videoId = explode( "v=", $url )[1];
      if ( strpos( $videoId, '&' ) !== false ) {
        $videoId = explode( "&", $videoId )[0];
      }
      $finalUrl .= 'https://www.youtube.com/embed/' . $videoId;
    } else {
      if ( strpos( $url, 'youtu.be/' ) !== false ) {
        //it is Youtube video
        $videoId = explode( "youtu.be/", $url )[1];
        if ( strpos( $videoId, '&' ) !== false ) {
          $videoId = explode( "&", $videoId )[0];
        }
        $finalUrl .= 'https://www.youtube.com/embed/' . $videoId;
      } else {
        //Enter valid video URL
      }
    }
  }

  return $finalUrl;
}

//endregion function for generating an embed link of an FB/Vimeo/Youtube Video

// region ADD Custom Menu Item checkbox to retrieve custom classes
add_action( 'wp_nav_menu_item_custom_fields', function ( $item_id, $item ) {
  $show_as_button = get_post_meta( $item_id, '_show-as-button', true );
  $two_columns    = get_post_meta( $item_id, '_two-columns', true );
  $show_as_icon   = get_post_meta( $item_id, '_show-as-icon', true );
  ?>
  <p class="codesign-two-columns description description-wide">
    <label for="codesign-menu-item-link-<?php echo $item_id; ?>">
      <input type="checkbox"
             id="codesign-menu-item-link-<?php echo $item_id; ?>"
             name="codesign-menu-item-link[<?php echo $item_id; ?>]"
        <?php checked( $two_columns, true ); ?>
      /><?php _e( 'Two Columns', 'codesign' ); ?>
    </label>
  </p>
  <p class="codesign-show-as-button description description-wide">
    <label for="codesign-menu-item-button-<?php echo $item_id; ?>">
      <input type="checkbox"
             id="codesign-menu-item-button-<?php echo $item_id; ?>"
             name="codesign-menu-item-button[<?php echo $item_id; ?>]"
        <?php checked( $show_as_button, true ); ?>
      /><?php _e( 'Show as a button', 'codesign' ); ?>
    </label>
  </p>
  <p class="codesign-show-as-icon description description-wide">
    <label for="codesign-menu-item-icon-<?php echo $item_id; ?>">
      <input type="checkbox"
             id="codesign-menu-item-icon-<?php echo $item_id; ?>"
             name="codesign-menu-item-icon[<?php echo $item_id; ?>]"
        <?php checked( $show_as_icon, true ); ?>
      /><?php _e( 'with icon?', 'codesign' ); ?>
    </label>
  </p>

  <?php
}, 10, 2 );

add_action( 'wp_update_nav_menu_item', function ( $menu_id, $menu_item_db_id ) {
  $button_value = ( isset( $_POST['codesign-menu-item-button'][ $menu_item_db_id ] ) &&
                    $_POST['codesign-menu-item-button'][ $menu_item_db_id ] == 'on' ) ? true :
    false;
  update_post_meta( $menu_item_db_id, '_show-as-button', $button_value );

  $link_value = ( isset( $_POST['codesign-menu-item-link'][ $menu_item_db_id ] ) &&
                  $_POST['codesign-menu-item-link'][ $menu_item_db_id ] == 'on' ) ? true : false;
  update_post_meta( $menu_item_db_id, '_two-columns', $link_value );

  $icon_value = ( isset( $_POST['codesign-menu-item-icon'][ $menu_item_db_id ] ) &&
                  $_POST['codesign-menu-item-icon'][ $menu_item_db_id ] == 'on' ) ? true : false;
  update_post_meta( $menu_item_db_id, '_show-as-icon', $icon_value );
}, 10, 2 );

add_filter( 'nav_menu_css_class', function ( $classes, $menu_item ) {
  $show_as_button = get_post_meta( $menu_item->ID, '_show-as-button', true );
  $two_columns    = get_post_meta( $menu_item->ID, '_two-columns', true );
  $show_as_icon   = get_post_meta( $menu_item->ID, '_show-as-icon', true );
  if ( $show_as_button ) {
    $classes[] = 'cta-button';
  }
  if ( $two_columns ) {
    $classes[] = 'two-columns';
  }
  if ( $show_as_icon ) {
    $classes[] = 'has-icon';
  }

  return $classes;
}, 10, 2 );
// endregion ADD Custom Menu Item checkbox to retrieve custom classes

// region AJAX load more functions
add_action( 'get_footer', 'add_admin_url' );
function add_admin_url() {
  wp_localize_script( 'theme-main', 'theme_ajax_object',
    array(
      'ajax_url'    => admin_url( 'admin-ajax.php' ),
      '_ajax_nonce' => wp_create_nonce( 'nonce_ajax_more_posts' ),
    )
  );
}

add_action( 'wp_ajax_more_posts', 'more_posts' );
add_action( 'wp_ajax_nopriv_more_posts', 'more_posts' );
function more_posts() {

  if ( ! isset( $_POST['_ajax_nonce'] ) || ! wp_verify_nonce( sanitize_key( $_POST['_ajax_nonce'] ), 'nonce_ajax_more_posts' ) ) {
    return wp_send_json_error( esc_html__( 'Number not only once is invalid', 'cheq' ), 404 );
  }


  $args     = json_decode( stripcslashes( trim( $_POST['args'], '"' ) ), true );
  $template = $_POST['template'];


  $posts_query = new WP_Query( $args );
  header( 'X-WP-arg-pages: ' . ( $args['paged'] ?: 1 ) );
  header( 'X-WP-Has-More-Pages: ' . ( $posts_query->max_num_pages - ( $args['paged'] ?: 1 ) > 0 ) );
  header( 'X-WP-Total-Pages: ' . ( $posts_query->max_num_pages ) );
//var_dump($args);
//return

  ob_start();
  while ( $posts_query->have_posts() ):$posts_query->the_post();
    get_template_part( $template );
  endwhile;
  $posts_out = ob_get_clean();
  wp_reset_postdata();

  wp_send_json_success( $posts_out, 200 );


}

// endregion AJAX load more functions

//region THE SPEED OPTIMIZATION PARADISE
//region Remove emoji_icons from head
function disable_wp_emoji_icons() {
  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
}

add_action( 'init', 'disable_wp_emoji_icons' );
//endregion Remove emoji_icons from head
//region Remove [dashicons - admin-bar - duplicate-post - yoast-seo-adminbar - wp-block-library-theme - wp-block-library - wc-block-style ] CSS from loading on the frontend
function smartwp_remove_wp_block_library_css() {
  if ( ! is_user_logged_in() ) {
    wp_dequeue_style( 'dashicons' );
    wp_dequeue_style( 'admin-bar' );
    wp_dequeue_style( 'duplicate-post' );
    wp_dequeue_style( 'yoast-seo-adminbar' );
  }
  wp_dequeue_style( 'wp-block-library' );
  wp_dequeue_style( 'wp-block-library-theme' );
  wp_dequeue_style( 'wc-block-style' );
}

add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );
//endregion Remove [dashicons - admin-bar - duplicate-post - yoast-seo-adminbar - wp-block-library-theme - wp-block-library - wc-block-style ] CSS from loading on the frontend
//region Remove JQuery migrate
function remove_jquery_migrate( $scripts ) {
  if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
    $script = $scripts->registered['jquery'];

    if ( $script->deps ) { // Check whether the script has any dependencies
      $script->deps = array_diff( $script->deps, array(
        'jquery-migrate'
      ) );
    }
  }
}

add_action( 'wp_default_scripts', 'remove_jquery_migrate' );
//endregion Remove JQuery migrate
//region Eliminate render-blocking resources
if ( ! is_admin() ) {
  $deferredScriptHandles = [ 'theme-main' ];
  $deferredStyleHandles  = ['theme-main'];

  add_filter( 'script_loader_tag', function ( $tag, $handle ) {
    global $deferredScriptHandles;
    if ( ! in_array( $handle, $deferredScriptHandles ) ) {
      return $tag;
    }

    return str_replace( ' src', ' defer="defer" src', $tag );
  }, 10, 2 );
  add_filter( 'style_loader_tag', function ( $tag, $handle ) {
    global $deferredStyleHandles;
    if ( ! in_array( $handle, $deferredStyleHandles ) ) {
      return $tag;
    }

    return str_replace( ' rel',
        ' media="print" onload="this.onload=null;this.media=\'all\'" rel',
        $tag ) . "<noscript>" . $tag . '</noscript>';
  }, 10, 2 );


  if ( $_SERVER['REQUEST_URI'] == '/' ) {
    function remove_jquery() {
      wp_dequeue_script( 'jquery-core' );
    }

    add_action( 'wp_print_scripts', 'remove_jquery' );
  }
}
//endregion Eliminate render-blocking resources
//endregion THE SPEED OPTIMIZATION PARADISE

//region Check Rgb Color That's Return number from 0 to 255
function get_brightness( $hex ) {
  $hex = str_replace( '#', '', $hex );
  $c_r = hexdec( substr( $hex, 0, 2 ) );
  $c_g = hexdec( substr( $hex, 2, 2 ) );
  $c_b = hexdec( substr( $hex, 4, 2 ) );

  return ( ( $c_r * 299 ) + ( $c_g * 587 ) + ( $c_b * 114 ) ) / 1000;
}

//endregion Check Rgb Color That's Return number from 0 to 255

//region Change WP dashboard colors on localhost only
if ( $_SERVER['HTTP_HOST'] == 'localhost' ) {

  add_filter( 'get_user_option_admin_color', 'update_user_option_admin_color', 5 );

  function update_user_option_admin_color( $color_scheme ) {
    $color_scheme = 'modern';

    return $color_scheme;
  }
}
//endregion Change WP dashboard colors on localhost only
