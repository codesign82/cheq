<?php
get_header();
$author = get_queried_object();
$author_id = $author->ID;
// Get user object
$recent_author = get_user_by('ID', $author_id);
// Get user display name
$author_display_name = $recent_author->display_name;
?>

<?php
get_footer();