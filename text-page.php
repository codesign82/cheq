<?php
/*
 * Template Name: text Page
 * */


$max_width = get_field('max_width');


get_header(); ?>


  <style>
    @media screen and (min-width: 992px) {
      .text-page{
        max-width: <?=$max_width?>% !important;
      }
    }
  </style>

<?php while (have_posts()) : the_post(); ?>
  <div class="container page-wrapper">
    <div class="text-page">
      <?php the_content(); ?>
    </div>
  </div>
<?php endwhile; ?>

<?php get_footer();
